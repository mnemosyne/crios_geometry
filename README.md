# Name of library

## Description




## Installation

### from gitlab

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/crios_geometry.git
```

Ou

```
pip install git+ssh://git@gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/crios_geometry.git
```

### test install

Get install directory
```
pip show 
```

Use pytest on crios_geometry path
```
pytest --doctest-modules /path_wherre_pip_install/site-packages/
```



## Usage


```

```

## License
GNU GPL

