"""
Provide some utils, relations for geometrical applications
 - 2d
 - 3d
 - spatial: maps, cartograhpy, make and use different spatial object
"""
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

from crios_geometry.cst import *
from crios_geometry.utils import *
from crios_geometry.io import *
from crios_geometry.spatial.grids import *
from crios_geometry.spatial.mapping import *
from crios_geometry.spatial.objects import *
from crios_geometry.spatial.points import *

# ~ from japet_misc import todo_msg
# ~ print(todo_msg("Move sphere from facts, solar etc ......", __file__))

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
