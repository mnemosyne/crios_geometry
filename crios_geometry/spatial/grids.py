"""
Grids classes
"""


##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##


#packages python
import os
import argparse
import doctest
from collections.abc import Iterable

import numpy
import pandas
import scipy.stats
import matplotlib
from matplotlib import pyplot
#~ from mpl_toolkits.basemap import maskoceans

import shapely
import shapely.geometry

#packages perso
from crios_geometry.cst import XKEY, YKEY, XY_METHODS, XY_CORNERS, XY_PARTICULAR_POSITIONS
from crios_geometry.spatial.mapping import utm_proj, is_projobj
from crios_geometry.utils import distance, particular_point
from crios_geometry.spatial.objects import Point, Polygon

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##



##======================================================================================================================##
##                EXAMPLES FUNCTIONS                                                                                    ##
##======================================================================================================================##

def example_zgrid(xs=(-10, 10), ys=(-5, 10), random=True, not_reg=False):
    """
    Renvoie un objet zgrid
    >>> example_zgrid().__class__ == ZGrid
    True
    >>> example_zgrid(random = False).__class__ == ZGrid
    True
    """
    grid = example_grid(xs, ys, not_reg=not_reg)

    if random:
        #~ size = grid.columns.size * grid.index.size
        z_grid = scipy.stats.norm().rvs(grid.shape)
        #~ z_grid = z_grid.reshape(grid.index.size, grid.columns.size)
    else:
        z_grid = grid.meshgrid(XKEY) * grid.meshgrid(YKEY)

    res = ZGrid(z_grid, index=grid.index, columns=grid.columns)

    return res

def example_grid(xs=(0, 1), ys=(0, 1), not_reg=False):
    """
    >>> example_grid().__class__ == EmptyGrid
    True
    >>> example_grid(not_reg=True).__class__ == EmptyGrid
    True

    """
    res = EmptyGrid(xlim=xs, ylim=ys, dx=0.5)
    if not_reg:
        x = numpy.exp(res.columns)
        res = EmptyGrid(x=x, y=res.index)

    return res






    

##======================================================================================================================##
##                                ZGRID                                                                                 ##
##======================================================================================================================##

class ZGrid(pandas.DataFrame):
    """
    >>> example_zgrid().__class__ == ZGrid
    True
    >>> zg1 = example_zgrid(xs=(2, 3), ys=(0, 1), random = False)
    >>> zg1.regular()
    True
    >>> zg2 = example_zgrid(xs=(1,2), ys=(2, 3), random = False)
    >>> zg1
         2.0   2.5  3.0
    0.0  0.0  0.00  0.0
    0.5  1.0  1.25  1.5
    1.0  2.0  2.50  3.0

    >>> zg2
         1.0   1.5  2.0
    2.0  2.0  3.00  4.0
    2.5  2.5  3.75  5.0
    3.0  3.0  4.50  6.0
    >>> zg1 + 1
         2.0   2.5  3.0
    0.0  1.0  1.00  1.0
    0.5  2.0  2.25  2.5
    1.0  3.0  3.50  4.0
    >>> zg1 - 1
         2.0   2.5  3.0
    0.0 -1.0 -1.00 -1.0
    0.5  0.0  0.25  0.5
    1.0  1.0  1.50  2.0
    >>> zg1 * 2
         2.0  2.5  3.0
    0.0  0.0  0.0  0.0
    0.5  2.0  2.5  3.0
    1.0  4.0  5.0  6.0
    >>> zg1 / 2
         2.0    2.5   3.0
    0.0  0.0  0.000  0.00
    0.5  0.5  0.625  0.75
    1.0  1.0  1.250  1.50
    >>> zg2 = zg1 * 100
    >>> zg2
           2.0    2.5    3.0
    0.0    0.0    0.0    0.0
    0.5  100.0  125.0  150.0
    1.0  200.0  250.0  300.0
    
    >>> zg1 + zg2
           2.0     2.5    3.0
    0.0    0.0    0.00    0.0
    0.5  101.0  126.25  151.5
    1.0  202.0  252.50  303.0

    
    >>> zg1 - zg2
           2.0     2.5    3.0
    0.0    0.0    0.00    0.0
    0.5  -99.0 -123.75 -148.5
    1.0 -198.0 -247.50 -297.0
    >>> zg1 * zg1
         2.0     2.5   3.0
    0.0  0.0  0.0000  0.00
    0.5  1.0  1.5625  2.25
    1.0  4.0  6.2500  9.00
    >>> zg2 = zg1 + 1
    >>> zg2
         2.0   2.5  3.0
    0.0  1.0  1.00  1.0
    0.5  2.0  2.25  2.5
    1.0  3.0  3.50  4.0
    >>> zg3 =zg2 * 100
    >>> zg3
           2.0    2.5    3.0
    0.0  100.0  100.0  100.0
    0.5  200.0  225.0  250.0
    1.0  300.0  350.0  400.0
    >>> zg3 / zg2
           2.0    2.5    3.0
    0.0  100.0  100.0  100.0
    0.5  100.0  100.0  100.0
    1.0  100.0  100.0  100.0
    
    >>> zg1
         2.0   2.5  3.0
    0.0  0.0  0.00  0.0
    0.5  1.0  1.25  1.5
    1.0  2.0  2.50  3.0

    >>> zg1.expand_grid(1)   
          1.0  2.0   2.5  3.0  4.0
    -1.0  NaN  NaN   NaN  NaN  NaN
     0.0  NaN  0.0  0.00  0.0  NaN
     0.5  NaN  1.0  1.25  1.5  NaN
     1.0  NaN  2.0  2.50  3.0  NaN
     2.0  NaN  NaN   NaN  NaN  NaN
    """
    
    def __init__(self, *args, **kwargs):
        """
        Init
        """
        if "gridx" in kwargs:
            gridx = kwargs.pop("gridx")
            gridy = kwargs.pop("gridy")
            assert len(gridx.shape) == 2, "pb dim exp 2, got {0}".format(gridx.shape)
            assert gridy.shape == gridx.shape, "pb dim x and y st have same dim: got {0} and {1}".format(gridx.shape, gridy.shape)
            addgd = True
        else:
            addgd = False
        
        pandas.DataFrame.__init__(self, *args, **kwargs)
        
        if addgd:
            self.gridx = gridx
            self.gridy = gridy

    
    def regular(self):
        """
        See main doctest
        """
        res = not hasattr(self, "gridx")
        #~ res = self.gridx is None
        return res

    def meshgrid(self, axe):
        """
        meshgrid for pcontour plot
        See main doctest
        """
        reg = self.regular()
        if axe == XKEY:
            if reg:
                vals = [[ix for ix in self.columns] for iy in self.index]
            else:
                vals = self.gridx
        elif axe == YKEY:
            if reg:
                vals = [[iy for ix in self.columns] for iy in self.index]
            else:
                vals = self.gridy
        else:
            raise ValueError("Dont know what to do with {0}".format(axe))
            
        res = pandas.DataFrame(vals, index=self.index, columns=self.columns)
        return res
    
    def pcolorgrid(self):
        """
        meshgrid for pcolor function
        See main Doctest
        """
        assert self.regular(), "not implemented for non regular grid"
        ys = pandas.Series(self.index.values)
        xs = pandas.Series(self.columns.values)

        #work on x
        newx = list(xs.rolling(2, center=True).mean().dropna())
        difx = numpy.diff(xs)
        newx = [newx[0] - difx[0]] + newx +  [newx[-1] + difx[-1]]
        
        #work on y
        newy = list(ys.rolling(2, center=True).mean().dropna())
        dify = numpy.diff(ys)
        newy = [newy[0] - dify[0]] + newy +  [newy[-1] + dify[-1]]
        
        res = EmptyGrid(x=newx, y=newy)
        return res
        
    def compute_distances(self, *args):
        """
        Compute distances with args given
        >>> grid =EmptyGrid(xlim = (0, 2), ylim= (0, 3), dx = 0.5)
        >>> ds = grid.compute_distances(0, 0)
        >>> ds.round(2)
             0.0   0.5   1.0   1.5   2.0
        0.0  0.0  0.50  1.00  1.50  2.00
        0.5  0.5  0.71  1.12  1.58  2.06
        1.0  1.0  1.12  1.41  1.80  2.24
        1.5  1.5  1.58  1.80  2.12  2.50
        2.0  2.0  2.06  2.24  2.50  2.83
        2.5  2.5  2.55  2.69  2.92  3.20
        3.0  3.0  3.04  3.16  3.35  3.61
        
        >>> grid.extraction(xlim=(0.1,1.6), ylim=(None, 2))
             0.5  1.0  1.5
        0.0  NaN  NaN  NaN
        0.5  NaN  NaN  NaN
        1.0  NaN  NaN  NaN
        1.5  NaN  NaN  NaN
        2.0  NaN  NaN  NaN

        >>> grid.extraction(xlim=(0.1,None), ylim=(0.9, 2))
             0.5  1.0  1.5  2.0
        1.0  NaN  NaN  NaN  NaN
        1.5  NaN  NaN  NaN  NaN
        2.0  NaN  NaN  NaN  NaN

        
        """
        if len(args) == 1:
            args = args[0]
            assert isinstance(args, Point), "Point expected got {0}".format(args)
            x = args.columns.values
            y = args.y
        
        elif len(args) == 2:
            x, y = args
        else:
            raise ValueError("Exp point or x, y values, got {0}".format(args))
        
        gridx = self.meshgrid(axe=XKEY)
        gridy = self.meshgrid(axe=YKEY)
        #use return grid
        dx = gridx - x
        dy = gridy - y
        
        res = numpy.hypot(dx, dy)
        
        return res

    def plot(self, **kwargs):
        """
        >>> from japet_misc import show_all
        >>> r=EmptyGrid(xlim = (0, 1), ylim = (0, 1), dx = 0.2).plot()
        >>> show_all()
        """
        argdic = {"marker" : "o", 'ms' : 4, "ls": "none"}
        argdic.update(kwargs)
        
        x = self.meshgrid(axe=XKEY)
        y = self.meshgrid(axe=YKEY)
        x = x.values.reshape(x.size)
        y = y.values.reshape(y.size)

        res = pyplot.plot(x, y, **argdic)[0]

        return res
            
    def plot_cgrid(self, **kwargs):
        """
        >>> from japet_misc import show_all
        >>> grid = EmptyGrid( xlim = (0, 2), ylim =(0, 3), dx = 0.5)
        >>> f = pyplot.figure()
        >>> xy=grid.plot_cgrid()
        >>> l=grid.plot()
        >>> show_all()
        """
        argdic = {"color" : "k", 'ls' : "-", "lw" : 1}
        argdic.update(kwargs)
        
        cgrid = self.pcolorgrid()
        xgrid = cgrid.meshgrid(XKEY).values
        ygrid = cgrid.meshgrid(YKEY).values
        shape = xgrid.shape
        
        ylines = [pyplot.plot(xgrid[irow, :], ygrid[irow, :], **argdic)[0] for irow in range(shape[0])]
        xlines = [pyplot.plot(xgrid[:, icol], ygrid[:, icol], **argdic)[0] for icol in range(shape[1])]
        
        res = {"xlines" : xlines, "ylines" : ylines}
        
        return res
            
    def expand_grid(self, delta):
        """
        Exand the grid
        """
        xs = sorted(self.columns)
        ys = sorted(self.index)
        xf = xs[0] - delta
        yf = ys[0] - delta
        xl = xs[-1] + delta
        yl = ys[-1] + delta
        
        newxs = [xf] + xs + [xl]
        newys = [yf] + ys + [yl] 
            
        res = ZGrid(self, index=newys, columns=newxs)
        
        return res
            
    def use_projection(self, proj_obj):
        """
        Function doc
        >>> x = (-1, 1)
        >>> y = (-0.5,0.5)
        >>> g = EmptyGrid(xlim = x, ylim = y, dx = 1)
        >>> b = utm_proj(x,y)
        >>> ng = g.use_projection(b)
        >>> numpy.round(ng.gridx[(0,-1), (0,-1)]/ 1000)
        array([-111.,  111.])
        >>> numpy.round(ng.gridy[(0,-1), (0,-1)] / 1000)
        array([-55.,  55.])
        """
        assert is_projobj(proj_obj) or proj_obj is None, "grid type proj_obj : %s %s" % (type(proj_obj), proj_obj)
        x = self.meshgrid(XKEY).values.flatten()
        y = self.meshgrid(YKEY).values.flatten()
        pgrid = proj_obj(x, y)
        xn = pgrid[0].reshape(self.shape)
        yn = pgrid[1].reshape(self.shape)
        res = ZGrid(self.values, gridx=xn, gridy=yn)

        return res

    def to_polygon(self, use_cgrid=True):
        """
        Return a poly which contains the grid
        
        >>> g = EmptyGrid(xlim = (0,10), ylim = (0,10), dx = 1)
        >>> p = g.to_polygon()
        >>> type(p) == Polygon
        True
        >>> p.lims()
                x     y
        min  -0.5  -0.5
        max  10.5  10.5
        >>> p.area
        121.0
        >>> p = g.to_polygon(use_cgrid=False)
        >>> type(p) == Polygon
        True
        >>> p.lims()
                x     y
        min   0.0   0.0
        max  10.0  10.0
        >>> p.area
        100.0
        """
        if use_cgrid:
            cgrid = self.pcolorgrid()
            xgrid = cgrid.meshgrid(XKEY).values
            ygrid = cgrid.meshgrid(YKEY).values
        else:
            xgrid = self.meshgrid(XKEY).values
            ygrid = self.meshgrid(YKEY).values
        
        shape = ygrid.shape
        
        ny = shape[0]
        nx = shape[1]

        xs = []
        ys = []

        indxs = [numpy.arange(0, nx), -1, numpy.arange(nx-1, 0, -1), 0]
        indys = [0, numpy.arange(1, ny), -1, numpy.arange(ny-1, 0, -1)]

        for indy, indx in zip(indys, indxs):
            if isinstance(indx, int):
                indx = numpy.array([indx] * indy.size)
            if isinstance(indy, int):
                indy = numpy.array([indy] * indx.size)
            
            x = xgrid[indy, indx]
            y = ygrid[indy, indx]

            xs = numpy.concatenate([xs, x])
            ys = numpy.concatenate([ys, y])

        #~ pts = 
        res = Polygon([Point(x, y) for x, y in zip(xs, ys)])
        
        
        return res        
    
    def mask_zgrid(self):
        """
        Use for plot
        >>> g = EmptyGrid(xlim = (0,10), ylim = (0,10), dx = 1)
        >>> x = g.meshgrid('x')
        >>> y = g.meshgrid('y')
        >>> z = x * y
        >>> zg = ZGrid(z)
        >>> mz = zg.mask_zgrid()
        >>> (mz== z).all().all()
        True
        >>> z.loc[0, 0] = numpy.nan
        >>> mz = zg.mask_zgrid()
        >>> (mz.data == z).all().all()
        False
        >>> type(mz) == numpy.ma.core.MaskedArray
        True
        >>> numpy.isnan(mz.data[0,0])
        True
        >>> nonan = numpy.where(mz.mask == False)
        >>> numpy.all(z.values[nonan] == mz.data[nonan])
        True
        """
        res = self.values
        nans = self.isnull()
        if nans.sum().sum() > 0:
            #~ print("check mask")
            res = numpy.ma.array(res, mask=nans)
        
        return res
    
    
    #COMPUTATION    #
    #---------------#
    def centroid(self):
        """
        Function doc
        >>> gd = example_grid((0,10), (10,20))
        >>> print(gd.centroid())
        POINT (5 15)
        """
        res = self.to_polygon().centroid
        return res
        
    def particular_point(self, method):
        """
        >>> gd = example_grid((0,10), (10,20))
        >>> pandas.Series({m: gd.particular_point(m) for m in XY_METHODS}).sort_index()
        center center     POINT (5 15)
        center left       POINT (0 15)
        center rigth     POINT (10 15)
        lower center      POINT (5 10)
        lower left        POINT (0 10)
        lower rigth      POINT (10 10)
        upper center      POINT (5 20)
        upper left        POINT (0 20)
        upper rigth      POINT (10 20)
        dtype: object
        """
        res = particular_point(x=self.columns.values, y=self.index.values, method=method)
        return res
        
        

    def delim_zones(self, xlength, ylength, method="all"):
        """
        Function doc
        >>> g=example_grid((0,10), (10,20))
        >>> g.delim_zones(1,1)
                                          x                        y
        centroid      slice(4.5, 5.5, None)  slice(14.5, 15.5, None)
        lower left    slice(0.0, 1.0, None)  slice(10.0, 11.0, None)
        lower rigth  slice(9.0, 10.0, None)  slice(10.0, 11.0, None)
        upper left    slice(0.0, 1.0, None)  slice(19.0, 20.0, None)
        upper rigth  slice(9.0, 10.0, None)  slice(19.0, 20.0, None)
        """
        if method is None or method == 'all':
            res = self.delim_zones(xlength=xlength, ylength=ylength, method=XY_PARTICULAR_POSITIONS)
        elif isinstance(method, Iterable) and not isinstance(method, str):
            dicres = {imeth : self.delim_zones(xlength=xlength, ylength=ylength, method=imeth) for imeth in method}
            res = pandas.DataFrame(dicres, index=(XKEY, YKEY))
            res = res.transpose()
            res = res.sort_index()
        else:
            center = self.centroid()
            if method in XY_CORNERS:
                pt = self.particular_point(method=method)
                x0 = pt.x
                y0 = pt.y
                xsign = numpy.sign(center.x-x0)
                ysign = numpy.sign(center.y-y0)
                # ~ print(xsign, ysign)
                x1 = x0 + xsign * xlength
                y1 = y0 + ysign * ylength
                xlim = sorted([x0, x1])
                ylim = sorted([y0, y1])
            else:
                assert method in ("center", "center center", "centroid")
                signs = [-1, 1]
                dx = xlength/2
                dy = ylength/2
                xlim = [center.x + i*dx for i in signs]
                ylim = [center.y + i*dy for i in signs]
            
            # ~ print(xlim, ylim)
            dic = {XKEY: xlim, YKEY: ylim}
            res = {k : slice(*v) for k, v in dic.items()}
            
        return res
        
        
        
        
        

    #PLOT           #
    #---------------#

    def pcolor(self, **kwargs):
        """
        >>> from japet_misc import *
        
        #~ >>> g =example_zgrid()
        #~ >>> f =pyplot.figure()
        #~ >>> vn, vx = normalize_list(numpy.array(g).reshape(g.size), factor = 1)
        #~ >>> bds = numpy.arange(vn, vx, 1)
        
        #~ >>> ncol = len(bds) - 1
        #~ >>> nm = matplotlib.colors.BoundaryNorm(bds, ncolors = ncol)
        #~ >>> fig = pyplot.figure()
        #~ >>> cax = pyplot.axes([0.85,0.1,0.1,0.8])
        #~ >>> ax = pyplot.axes([0.1,0.1,0.7,0.8])        
        #~ >>> cm = get_colormap('viridis', ncol = ncol)
        #~ >>> r=g.pcolor(cmap = cm, norm = nm)
        #~ >>> cb = pyplot.colorbar(r, cax =cax)
        
        >>> g=example_zgrid(random = False)
        >>> vn, vx = normalize_list(numpy.array(g).reshape(g.size), factor = 1)
        >>> bds = auto_bounds((vn, vx))
        >>> fig = pyplot.figure()
        >>> cax = pyplot.axes([0.85,0.1,0.1,0.8])
        >>> ax = pyplot.axes([0.1,0.1,0.7,0.8]) 
        >>> cb = plot_colorbar(bds, cmap = "viridis",cax = cax)       
        >>> r=g.pcolor(colorbar = cb)
        >>> l=g.contour(levels = bds)

        >>> extd = True
        >>> cmnm = "viridis"

        >>> fig = pyplot.figure()
        >>> ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))
        >>> axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))

        >>> bds = [0, 23, 43, 56, 57, 89]
        >>> cb = plot_colorbar(bds, cmap = cmnm, extend = extd, cax = axcb, orientation = "vertical")
        >>> axcb.grid(color='k', linestyle='-', linewidth=1)
        >>> pyplot.sca(ax)
        >>> cf = g.pcolor(colorbar = cb)
        >>> cl = g.contour(levels = bds)
        
        
        >>> show_and_close(1)
        """
        if "colorbar" in kwargs:
            #~ assert len(kwargs) == 1
            cb = kwargs.pop("colorbar")
            kwargs.update(dict(cmap=cb.cmap, norm=cb.norm))
        
        argdic = {"alpha" : 1, 'edgecolors' : "none"} #,  'extend':'both'
        argdic.update(kwargs)
        
        #ZGRID AND GRID
        z_grid = self.mask_zgrid()
        pcol_g = self.pcolorgrid()
        pcol_x = pcol_g.meshgrid(XKEY)
        pcol_y = pcol_g.meshgrid(YKEY)

        #PCOLORS
        res = pyplot.pcolor(pcol_x, pcol_y, z_grid, **argdic)

        return res

    def contour(self, fs=12, fmt='%0.2g', **kwargs):
        """
        >>> from japet_misc import *
        >>> g=example_zgrid(random = False)
        >>> f =pyplot.figure()
        >>> r=g.contour()
        >>> f =pyplot.figure()
        >>> r=g.contour(lw = 5)
        >>> f =pyplot.figure()
        >>> r=g.contour(fs = 20)
        >>> f =pyplot.figure()
        >>> r=g.contour(color ="r")
        >>> f =pyplot.figure()
        >>> r=g.contour( levels=(-200, -150, 0, 20, 34, 56))
        >>> f =pyplot.figure()
        >>> r=g.contour( levels=numpy.array([-200, -150, 0, 20, 34, 56]))
        >>> f =pyplot.figure()
        >>> r=g.contour(lw = 2, fs = 15, color = "b", levels = [-25, 0, 25, 50, 100], project = True, fmt = "%0.2f", mask = True)
        >>> show_and_close(0.1)
        """
        argdic = {'linewidths' : 1, "colors" : 'k', 'origin' :'lower'}
        argdic.update(kwargs)
        
        #CHECK KWARG FOR CONTOUR
        lw = argdic.pop("linewidths")
        assert isinstance(lw, (float, int)), 'pb entree demande {0} got {1.__class__} : {1}'.format("nombre", lw)
        assert isinstance(fs, (float, int)) or fs is False, 'numb exp got {0.__class__} : {0}'.format(fs)
        
        #ZGRID AND GRID
        z_grid = self.mask_zgrid()
        pcon_x = self.meshgrid(XKEY)
        pcon_y = self.meshgrid(YKEY)

        #CONTOUR
        pcont = pyplot.contour(pcon_x, pcon_y, z_grid, **argdic)

        #TEXT LABELS
        if fs != False:
            clab = pyplot.clabel(pcont, inline=1, fontsize=fs, fmt=fmt)
            for text in clab:
                text.set_color(argdic["colors"])
        else:
            clab = None

        res = {"contour_line" : pcont, "contour_lab" : clab}
        return res

    def contourf(self, **kwargs):
        """
        #~ Function doc
        >>> from japet_misc import *
        >>> g=example_zgrid(random = False)
        
        #~ >>> vn, vx = normalize_list(numpy.array(g).reshape(g.size), factor = 1)
        #~ >>> lvls = auto_bounds((vn, vx))
        #~ >>> ncol = len(lvls) - 1
        #~ >>> nm = matplotlib.colors.BoundaryNorm(lvls, ncolors = ncol)
        #~ >>> fig = pyplot.figure()
        #~ >>> cm = get_colormap('viridis', ncol = ncol)
        #~ >>> cm.N
        #~ 10
        #~ >>> (nm.boundaries == lvls).all()
        #~ True
        #~ >>> cax = pyplot.axes([0.85,0.1,0.1,0.8])
        #~ >>> ax = pyplot.axes([0.1,0.1,0.7,0.8])
        #~ >>> r = g.contourf(cmap = cm, norm = nm, levels = lvls)
        #~ >>> cb = pyplot.colorbar(r, cax =cax)
        #~ >>> (cb.norm.boundaries == lvls).all()
        #~ True
        #~ >>> len(cb.cmap.colors)
        #~ 10
        
        #~ >>> lvls = [0,5,6,9,15]
        #~ >>> ncol = len(lvls) - 1
        #~ >>> nm = matplotlib.colors.BoundaryNorm(lvls, ncolors = ncol)
        #~ >>> fig = pyplot.figure()
        #~ >>> cm = get_colormap('viridis', ncol = ncol)
        #~ >>> l = g.contour(levels = lvls)
        #~ >>> cm.N
        #~ 4
        #~ >>> nm.boundaries
        #~ array([ 0,  5,  6,  9, 15])
        #~ >>> cax = pyplot.axes([0.85,0.1,0.1,0.8])
        #~ >>> ax = pyplot.axes([0.1,0.1,0.7,0.8])
        #~ >>> r = g.contourf(cmap = cm, norm = nm, levels = lvls)
        #~ >>> cb = pyplot.colorbar(r, cax =cax)
        #~ >>> l = g.contour(levels = lvls)
        #~ >>> cb.norm.boundaries
        #~ array([ 0,  5,  6,  9, 15])
        #~ >>> len(cb.cmap.colors)
        #~ 4
        
        #~ >>> lvls = auto_bounds((vn, vx))
        #~ >>> ncol = len(lvls) - 1
        #~ >>> nm = matplotlib.colors.BoundaryNorm(lvls, ncolors = ncol)
        #~ >>> fig = pyplot.figure()
        #~ >>> cm = get_colormap('viridis', ncol = ncol)
        #~ >>> cm.N
        #~ 10
        #~ >>> (nm.boundaries == lvls).all()
        #~ True
        #~ >>> cax = pyplot.axes([0.85,0.1,0.1,0.8])
        #~ >>> ax = pyplot.axes([0.1,0.1,0.7,0.8])
        #~ >>> r = g.contourf(cmap = cm, norm = nm)
        #~ >>> cb = pyplot.colorbar(r, cax =cax)
        #~ >>> nlvls = r.levels
        #~ >>> nlvls
        #~ array([-100.,  -75.,  -50.,  -25.,    0.,   25.,   50.,   75.,  100.])
        #~ >>> l = g.contour(levels = nlvls)
        #~ >>> len(cb.cmap.colors)
        #~ 10
        
        >>> cmnm = "viridis"
        >>> extd = True

        >>> fig = pyplot.figure()
        >>> ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))
        >>> axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))

        >>> bds = [0, 23, 43, 56, 57, 89]
        >>> cb = plot_colorbar(bds, cmap = cmnm, extend = extd, cax = axcb, orientation = "vertical")
        >>> axcb.grid(color='k', linestyle='-', linewidth=1)
        >>> pyplot.sca(ax)
        >>> cb.extend
        'both'
        >>> cf = g.contourf(colorbar = cb)
        >>> cl = g.contour(levels = bds)

        >>> show_all()
        
        >>> show_and_close(0.1)
        """
        if "colorbar" in kwargs:
            #~ assert len(kwargs) == 1
            cb = kwargs.pop("colorbar")
            kwargs.update(dict(kwargs, cmap=cb.cmap, norm=cb.norm, levels=cb.norm.boundaries, extend=cb.extend))
            #~ print(kwargs)
        
        #KWARG TREAMENT
        argdic = {"alpha" : 1}#, 'extend':'both'} #extend='neither'
        argdic.update(kwargs)
        #~ print(argdic)
        
        #ZGRID and GRID
        z_grid = self.mask_zgrid()
        pcon_x = self.meshgrid(XKEY)
        pcon_y = self.meshgrid(YKEY)
        
        res = pyplot.contourf(pcon_x, pcon_y, z_grid, **argdic)
        return res

    def marker_plot(self, marker_scale):
        """       
        Plot by using mmakrker scale
        >>> from japet_misc import *
        >>> limits = (0, 1)
        >>> mkscale = MarkerScale(*limits)
        >>> mkscale = mkscale.add_property(ms = [8,10,12],marker = ["s","o","^"], mfc = ["r","k","c"], mew = 2)
        >>> zg = example_zgrid((-1,1),(0,3),random = False)
        >>> fig = pyplot.figure()        
        >>> a=zg.marker_plot(mkscale)
        >>> fig = pyplot.figure()

        >>> a = mkscale.plot()
        >>> show_and_close()
        """
        x = self.meshgrid(XKEY).values
        y = self.meshgrid(YKEY).values
        z = self.mask_zgrid()
        
        x = x.flatten()
        y = y.flatten()#.reshape(shp)
        z = z.flatten()#.reshape(shp)
        
        res = [pyplot.plot(ix, iy, **marker_scale.return_mk(iv)) for ix, iy, iv in zip(x, y, z)]
        
        return res


    def extraction(self, xlim=None, ylim=None, polygon=None):
        """
        Extract subgrid
        """
        # ~ import random
        # ~ print(random.random())
        if polygon:
            assert isinstance(polygon, (Polygon, shapely.geometry.Polygon))
            polygon = Polygon(polygon)
            polylims = polygon.lims()
            if not xlim:
                xlim = tuple(polylims[XKEY])
            if not ylim:
                ylim = tuple(polylims[YKEY]) 
        
        
        if xlim:
            assert isinstance(xlim, Iterable) and len(xlim) == 2
            xslc = slice(*xlim)
        else:
            xslc = slice(None)
            
        if ylim:
            assert isinstance(ylim, Iterable) and len(ylim) == 2
            yslc = slice(*ylim)
        else:
            yslc = slice(None)

        res = self.loc[yslc, xslc]

        if polygon:
            mask = EmptyGrid(x=res.columns, y=res.index)
            for ix in res.columns:
                for iy in res.index:
                    pt = Point(x=ix, y=iy)
                    if polygon.contains(pt):
                        mask.loc[iy,ix] = 1
            
            # ~ polyoks = coords.apply(lambda pt: , axis=1) 
            # ~ polyoks = polyoks[polyoks]
            
            res = res * mask

            
        return res


##======================================================================================================================##
##                EMPTY GRID                                                                                            ##
##======================================================================================================================##


class EmptyGrid(ZGrid):
    """
    >>> g = EmptyGrid(x = numpy.arange(3), y = numpy.arange(2))
    >>> g.shape
    (2, 3)
    >>> g
         0.0  1.0  2.0
    0.0  NaN  NaN  NaN
    1.0  NaN  NaN  NaN
    
    >>> grid = EmptyGrid(xlim = (0, 2), ylim = (0, 3), dx = 0.5)
    >>> all( EmptyGrid(xlim=(0, 1), ylim=(0, 1), dx=0.5).columns.values == numpy.array([0. , 0.5, 1. ]))
    True
    
    >>> all(EmptyGrid(xlim=(0, 1), ylim=(0, 1), dx=0.5, dy=0.2).index.values  - numpy.array([0. , 0.2, 0.4, 0.6, 0.8, 1. ]) < 1e-8)
    True
    
    >>> g = EmptyGrid(x=numpy.arange(10), y=numpy.arange(6))
    >>> all(g.columns.values  == numpy.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9.]))
    True
    >>> all(g.index.values ==  numpy.array([0., 1., 2., 3., 4., 5.]))
    True
    >>> g = EmptyGrid(xlim = (0, 9), ylim=(0, 5), dx=1)
    >>> all(g.columns.values  == numpy.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9.]))
    True
    >>> all(g.index.values  == numpy.array([0., 1., 2., 3., 4., 5.]))
    True
    >>> g = EmptyGrid(x=(0, 4, 5, 9), y=(0, 1, 5))
    >>> all(g.columns.values  == numpy.array([0., 4., 5., 9.]))
    True
    >>> all(g.index.values  == numpy.array([0., 1., 5.]))
    True
    
    >>> grid = EmptyGrid(xlim=(0, 1), ylim=(2, 3), dx=0.5)
    >>> (grid.meshgrid("x").values == numpy.array([[0. , 0.5, 1. ], [0. , 0.5, 1. ], [0. , 0.5, 1. ]])).all()
    True

    >>> (grid.meshgrid("y").values == numpy.array([[2. , 2. , 2. ],[2.5, 2.5, 2.5],[3. , 3. , 3. ]])).all()
    True
           
    >>> grid = EmptyGrid(xlim=(0, 1), ylim=(2, 3), dx=0.5, dy=0.5)
    >>> pcolgrid = grid.pcolorgrid()
    >>> (pcolgrid.meshgrid("x").values == numpy.array([[-0.25,  0.25,  0.75,  1.25], [-0.25,  0.25,  0.75,  1.25], [-0.25,  0.25,  0.75,  1.25], [-0.25,  0.25,  0.75,  1.25]])).all()
    True
    >>> (pcolgrid.meshgrid("y").values == numpy.array([[1.75, 1.75, 1.75, 1.75], [2.25, 2.25, 2.25, 2.25], [2.75, 2.75, 2.75, 2.75], [3.25, 3.25, 3.25, 3.25]])).all()
    True
    """
    
    def __init__(self, **kwargs):
        """ Class initialiser """
        
        if "x" in kwargs and "y" in kwargs:
            x = kwargs.pop("x")
            y = kwargs.pop("y")
        elif "xlim" in kwargs and "ylim" in kwargs and "dx" in kwargs:
            xlim = kwargs.pop("xlim")
            ylim = kwargs.pop("ylim")
            dx = kwargs.pop("dx")
            if "dy" in kwargs:
                dy = kwargs.pop("dy")
            else:
                dy = dx
                
            x = numpy.arange(xlim[0], xlim[-1]+dx, dx)
            y = numpy.arange(ylim[0], ylim[-1]+dy, dy)
            
        
        else:
            raise ValueError("dont know what to do with {0}".format(kwargs))
        
        assert not kwargs, "dont know what to do with {0}".format(kwargs)
        
        
        x = numpy.array(x, dtype=float)
        y = numpy.array(y, dtype=float)
        
        assert len(x.shape) == 1 and len(y.shape) == 1
        ZGrid.__init__(self, None, index=y, columns=x)
        
    
    

 
 


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    parser.add_argument('--pickle', default=False, action='store_true')
    parser.add_argument('--test_color_bar', default=False, action='store_true')
    parser.add_argument('--check_pcolor', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
    if opts.examples or opts.test_color_bar or opts.check_pcolor:
        from functions import close_all, normalize_list, plot_colorbar, show_all, show_and_close, get_colormap, extend_bds, symmetric_bounds
        #~ from spatialobj import load_dem_cmap
        close_all()
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:

        #BMAP           #
        #---------------#
        mg = example_zgrid(random=False)
        #~ bmap = mg.grid_obj.proj_obj

        #example        #
        #---------------#
        #~ from functions import *

        close_all()
        nnn = 20
        mlevels = numpy.linspace(0, 4000, nnn + 1)
        #~ mlevels = "uniform"
        dem = "dem"

        mys = (-50, 50)
        mxs = mys
        mg = example_zgrid(xs=mxs, ys=mys, random=False)
        xlm = normalize_list(mxs)
        ylm = normalize_list(mys)

        mg.values[mg.values < -5] = numpy.nan

        #~ mindex = numpy.where(mg < -5)
        #~ mg[mindex] = 
        #~ g.z_grid = 

        mfig = pyplot.figure()
        fig = pyplot.figure()
        cax = pyplot.axes([0.92, 0.1, 0.05, 0.8])
        ax = pyplot.axes([0.1, 0.1, 0.8, 0.8])
        mcb = plot_colorbar(*mlevels, cmap=dem, cax=cax)
        mcb.cmap.set_under('c')
        mcb.cmap.set_over('0.5')
        
        fills = mg.contourf(colorbar=mcb)
        r = mg.contour(levels=mlevels)
        pyplot.grid()
        pyplot.xlim(xlm)
        pyplot.ylim(ylm)


        show_all()

        fig = pyplot.figure()
        cax = pyplot.axes([0.92, 0.1, 0.05, 0.8])
        ax = pyplot.axes([0.1, 0.1, 0.8, 0.8])
        mcb = plot_colorbar(*mlevels, cmap=dem, cax=cax)
        mcb.cmap.set_under('c')
        mcb.cmap.set_over('0.5')
        r = mg.pcolor(colorbar=mcb)
        pyplot.grid()
        pyplot.xlim(xlm)
        pyplot.ylim(ylm)

        show_and_close()

    if opts.test_color_bar:
        
        #+++++++++++++++++++++++++++++++#
        #    #
        #+++++++++++++++++++++++++++++++#
        
        #BMAP           #
        #---------------#
        mg = example_zgrid(random=False)
        
        bm = utm_proj()

        #example        #
        #---------------#
        #~ from functions import *

        close_all()
        nnn = 5
        #~ dem = load_dem_cmap(nnn)
        mlevels = numpy.linspace(mg.values.min() + 10, mg.values.max() - 100, nnn)
        mlevels = extend_bds([0, 23, 43, 56, 57, 89])
        nnn = len(mlevels)
        #~ mlevels = "uniform"
        dem = "dem"
        
        fig = pyplot.figure()
        ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))
        cax1 = pyplot.axes((0.75, 0.1, 0.08, 0.8))
        #~ cax2 = pyplot.axes((0.9, 0.1, 0.08, 0.8))
        cm = get_colormap(name="viridis", ncol=nnn, extend=True)
        #~ cm.set_under('0.5')
        #~ cm.set_over('0.8')
        print(cm._rgba_over, cm._rgba_under)
        pyplot.sca(ax)
        cf = mg.contourf(cmap=cm, levels=mlevels[1:-1], norm=matplotlib.colors.BoundaryNorm(mlevels[1:-1], cm.N))
        cl = mg.contour(levels=mlevels)
        #~ cf = mg.contourf(cmap = cm, norm = matplotlib.colors.BoundaryNorm(mlevels, cm.N))
        #~ cf = mg.contourf(levels = numpy.arange(6), colors = ["w", "r", "g", "b", "k"], extend = "both")
        #~ ht = mg.contourf(colors = 'w', levels = [-1,1], hatches = ["//"], alpha = 1, extend = "neither", edgecolors = "k")
        clb = pyplot.colorbar(cf, cax=cax1, extend="both")
        

        cmnm = "viridis"
        for extd in (True, False):
            #~ extd = True

            fig = pyplot.figure()
            ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))
            axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))

            bds = [0, 23, 43, 56, 57, 89]
            #~ nbds = len(bds)
            #~ print("bds",nbds, ":", bds)
            #~ ncol = nbds - 1

            clb = plot_colorbar(bds, cmap=cmnm, extend=extd, cax=axcb, orientation="vertical")
            axcb.grid(color='k', linestyle='-', linewidth=1)
            pyplot.sca(ax)
            print(clb.extend)
            cf = mg.contourf(colorbar=clb)
            cl = mg.contour(levels=mlevels)

            show_all()
            
            
        cmnm = "viridis"
        for extd in (True, False):

            fig = pyplot.figure()
            ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))
            axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))

            bds = [0, 23, 43, 56, 57, 89]
            clb = plot_colorbar(bds, cmap=cmnm, extend=extd, cax=axcb, orientation="vertical")
            axcb.grid(color='k', linestyle='-', linewidth=1)
            pyplot.sca(ax)
            print(clb.extend)
            cf = mg.pcolor(colorbar=clb)
            cl = mg.contour(levels=mlevels)

            show_all()
        
        #+++++++++++++++++++++++++++++++#
        #    PICKLE                     #
        #+++++++++++++++++++++++++++++++#
        import dill

        if opts.pickle:
            zgrid = example_zgrid()
            fpc = os.path.join(os.getenv("HOME"), "zgrid.pic")
            with open(fpc, "wb") as picw:
                dill.dump(file=picw, obj=zgrid)

            with open(fpc, "rb") as picr:
                zg2 = dill.load(file=picr)
                
            print(zg2.grid_obj)
            print(zgrid.grid_obj == zg2.grid_obj)
            print(numpy.abs(zgrid - zg2).max())
            #~ print(fidf2.distribution)
            os.remove(fpc)

    #+++++++++++++++++++++++++++++++#
    #    CHECK PCOLOR               #
    #+++++++++++++++++++++++++++++++#
    if opts.check_pcolor:
        g = EmptyGrid(x=numpy.arange(5), y=numpy.arange(5, 15))

        zgrid = example_zgrid(random=False)
        bds = symmetric_bounds([0.1, 1, 5, 10, 20, 50, 100], add_center=False)
        for i in range(2):
            fig = pyplot.figure()
            cax = pyplot.axes([0.92, 0.1, 0.05, 0.8])
            ax = pyplot.axes([0.1, 0.1, 0.8, 0.8])
            clb = plot_colorbar(bds, cmap="diff", cax=cax)
            if i == 0:
                zgrid.pcolor(colorbar=clb)
            else:
                zgrid.contourf(colorbar=clb)
                zgrid.contour(levels=[0])
            pyplot.plot([0]*2, normalize_list(zgrid.index.values), color="k", lw=2)
            pyplot.plot(normalize_list(zgrid.columns.values), [0]*2, color="k", lw=2)

            show_all()
            
            
    #+++++++++++++++++++++++++++++++#
    #    OTHER                      #
    #+++++++++++++++++++++++++++++++#
    print(XY_METHODS)
