"""
Basic spatial objects : Point, Polygone ...
"""



##======================================================================================================================##
##                                PACKAGES IMPORT                                                                       ##
##======================================================================================================================##

#packages python
import os
import argparse
import doctest
import math
from collections.abc import Iterable
import pickle
import numpy
# ~ import scipy.stats


import shapely
import shapely.geometry
import shapely.ops
import shapely.wkt
# ~ import matplotlib
from matplotlib import pyplot
#~ from matplotlib import colorbar
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from pyproj import Proj

import pandas

from crios_geometry.cst import XKEY, YKEY, DEGREES_NAMES, ZKEY
from crios_geometry.utils import XYlims
from crios_geometry.spatial.mapping import cardinal_direction_from_angle





##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

DEFLINE = {'marker' : "", "ls" : "-", "lw" : 2}

##======================================================================================================================##
##                              EXAMPLES FUNCTIONS                                                                      ##
##======================================================================================================================##



def example_poly(regular=True):
    """
    >>> regpoly = example_poly(True)
    >>> nr_poly = example_poly(False)
    
    >>> tri = Polygon([Point([0, 3, 0][i], [1, 2, 3][i], [1, 2, 1][i]) for i in range(3)])
    >>> pt = tri.centroid
    >>> pt.x, pt.y
    (1.0, 2.0)
    """
    if regular:
        pts = [Point([0, 0, 1, 1][i], [0, 1, 1, 0][i]) for i in range(4)]
    else:
        xs = [0, 0, 4, 4, 1, 1, 4, 4] 
        ys = [0, 4, 4, 3, 3, 1, 1, 0]
        pts = [Point(x, y) for x, y in zip(xs, ys)]
    
    pts = MultiPoints(pts)
    res = Polygon(pts)

    #~ res.plot()
    
    return res
    
    

    



##======================================================================================================================##
##                OTHERS FUNCTIONS                                                                                      ##
##======================================================================================================================##

def distance_2pts(pt1, pt2):
    """
    Function doc
    >>> p1 = Point(x = 0, y = 0)
    >>> distance_2pts(p1, Point(x= 1, y =0))
    1.0
    >>> distance_2pts(p1, Point(x= 1, y =1)) == 2.**0.5
    True
    >>> distance_2pts(p1, Point(x = 0, y =1))
    1.0
    >>> distance_2pts(p1, Point(x = 3, y = 4))
    5.0
    """
    assert isinstance(pt1, Point), f"exp point got {pt1}, {pt1.__class__}"
    assert isinstance(pt2, Point), f"exp point got {pt2}, {pt2.__class__}"

    dx = pt1.x - pt2.x
    dy = pt1.y - pt2.y
    res = math.hypot(dx, dy)
    return res
    
def _return_polygon_list(*args):
    """
    Return a list of polygons
    >>> p1 = Polygon([Point(x,y) for x,y in zip([0,1,1],[0,1,0]) ])
    >>> p2 = Polygon([Point(x,y) for x,y in zip([1,1,2],[0,1,0]) ])
    >>> _return_polygon_list(p1, p2) == _return_polygon_list([p1, p2])
    True
    """
    if len(args) == 1:
        res = args[0]
    else:
        res = args
    
    res = list(res)
           
    are_polys = [isinstance(i, (Polygon, shapely.geometry.Polygon)) for i in res]
    assert all(are_polys), f'pb input : expected polygons got:{res}'
    return res

def reduce_polygons(*args):
    """
    >>> p1 = Polygon([Point(x,y) for x,y in zip([0,1,1],[0,1,0]) ])
    >>> p1.area
    0.5
    >>> p2 = Polygon([Point(x,y) for x,y in zip([1,1,2],[0,1,0]) ])
    >>> p2.area
    0.5
    >>> p3 = reduce_polygons(p1,p2)
    >>> type(p3) == shapely.geometry.polygon.Polygon
    True
    >>> p3.area
    1.0
    
    >>> p1 = Polygon([Point(x,y) for x,y in zip([0,1,1],[0,1,0]) ])
    >>> p1.area
    0.5
    >>> p2 = Polygon([Point(x,y) for x,y in zip([1.5,1.5,2.5],[0,1,0]) ])
    >>> p2.area
    0.5
    >>> p3 = reduce_polygons(p1,p2)
    >>> type(p3) == shapely.geometry.multipolygon.MultiPolygon
    True
    """
    objs = _return_polygon_list(*args)
    
    geoms = [g if g.is_valid else g.buffer(0) for g in objs]
    
    assert all(i for i in geoms if i.is_valid)
    
    res = shapely.ops.cascaded_union(geoms)
    
    return res

def main_polygon(*args):
    """
    
    >>> polys= [Point(i,i).buffer(abs(i)) for i in range(-5,5) if i != 0]
    >>> squares = [Point(i,i).buffer(abs(i), cap_style = 3) for i in range(-4,6) if i != 0]
    >>> mpoly = main_polygon(polys)
    >>> center = mpoly.centroid
    >>> round(center.x)
    -5
    >>> round(center.y)
    -5
    >>> a = math.pi * 5 **2
    >>> math.floor(a) < mpoly.area < math.ceil(a)
    True
    >>> mpoly = main_polygon(polys + squares)
    >>> center = mpoly.centroid
    >>> round(center.x)
    5
    >>> round(center.y)
    5
    >>> mpoly.area
    100.0
    """
    objs = _return_polygon_list(*args)
    areas = [i.area for i in objs]
    ind = numpy.argmax(areas)
    res = objs[ind]
    
    assert res.area == max(areas), "pb code"
    
    return res

def inter_polygons(*args):
    """
    
    >>> squares = [Point(i,i).buffer(1, cap_style = 3) for i in (0, 1)]
    >>> totarea = sum([i.area for i in squares])
    >>> new = inter_polygons(squares)
    >>> newarea = new.area
    >>> totarea > newarea
    True
    >>> totarea
    8.0
    >>> newarea
    1.0
    """
    objs = _return_polygon_list(*args)
    
    for iii, geom in enumerate(objs):
        if iii == 0:
            res = geom
        else:
            res = res.intersection(geom)

    return res


def parse_multipolygons(obj):
    """
    From a multipolygon to a polygon list
    """
    res = [Polygon(i) for i in obj]
    return res
    
def parse_polygons(*objs):
    """
    Polygon and multipolygon list to polygon list
    """
    if len(objs) == 1:
        objs = objs[0]
    else:
        pass
    
    assert isinstance(objs, Iterable)
    polys = [parse_multipolygons(i) if isinstance(i, shapely.geometry.MultiPolygon) else [Polygon(i)] for i in objs]
    res = list(numpy.concatenate(polys))
    return res
    
    
    
##======================================================================================================================##
##                POINT (s) classes                                                                                     ##
##======================================================================================================================##

class Point(shapely.geometry.Point):
    """ Objet point
    Entree:
    - x   :   valeur de abscisse, souvent la longitude
    - y   :   valeur de l'ordonee, souvent la latitude
    - z   :   valeur du point

    >>> p = Point(x=2, y=5., z = 216)
    >>> p.x, p.y
    (2.0, 5.0)
    >>> p.z
    216.0
    >>> p = Point(2, 5., 216)
    >>> p.x, p.y
    (2.0, 5.0)
    >>> p.z
    216.0
    >>> print(p)
         x    y      z
    0  2.0  5.0  216.0
    >>> p.to_df()
         x    y      z
    0  2.0  5.0  216.0

    >>> p = Point(1, 1)
    >>> p == Point(1.0, 1.0)
    True
    >>> p == Point(1.0, 1.01, 5)
    False
    
    >>> l = Point(1.0, 1.01, 5).plot()
    
    """
    def __init__(self, *args, **kwarg):
        """Init object   """
        z = 0 #default value

        if not args and kwarg:
            x = kwarg.pop("x")
            y = kwarg.pop("y")
            if "z" in kwarg:
                z = kwarg.pop("z")
        else:
            if len(args) == 1:
                pt = args[0]
                # ~ print(kwarg)
                
                assert isinstance(pt, shapely.geometry.Point), "exp point or args"
                x = pt.x
                y = pt.y
                if pt.has_z:
                    z = pt.z
                elif "z" in kwarg:
                    z = kwarg.pop("z")
                else:
                    pass
            elif len(args) == 2:
                x, y = args
                if "z" in kwarg:
                    z = kwarg.pop("z")
                
            elif len(args) == 3:
                x, y, z = args
            else:
                raise ValueError("exp x, y, z not more")

        if isinstance(z, str):
            shapely.geometry.Point.__init__(self, x, y, 0)
            self.txt = z
        else:
            z = float(z)
            shapely.geometry.Point.__init__(self, x, y, z)
            self.txt = None
        
        assert not kwarg, f"dont know what to do with {kwarg}"

    def __str__(self):
        """
        Print function
        """
        res = pandas.DataFrame.__str__(self.to_df())
        return res
            
    def to_df(self):
        """
        Retrun a DataFrame representation of the object
        """
        df = {XKEY : self.x, YKEY : self.y, ZKEY : self.z}
        df = {k : [v] for k, v in df.items()} 
        res = pandas.DataFrame(df, columns=(XKEY, YKEY, ZKEY))
        return res


    def use_projection(self, proj_obj, **kwargs):
        """
        >>> pt = Point(2, 5., 236)
        >>> bmap=Proj(llcrnrlon=0, llcrnrlat=0, urcrnrlon=1, urcrnrlat=1, proj='merc', lon_0=0, lat_0=0,ellps = 'WGS84')
        >>> npt = pt.use_projection(bmap)
        >>> round(npt.x / 1000)
        223
        >>> round(npt.y / 1000)
        554
        """
        xyproj = proj_obj(self.x, self.y, **kwargs)
        xproj, yproj = xyproj
        res = Point(x=xproj, y=yproj, z=self.z)
        return res
            
    def return_position_with_other_point(self, point, unit="deg"):
        """
        >>> p1 = Point(x = 0, y = 0)
        >>> p1.return_position_with_other_point(Point(x= 1, y =0)) == {'angle': 360.0, 'radius': 1.0}
        True
        """
        dx = point.x - self.x
        dy = point.y - self.y
        
        radius = math.hypot(dx, dy)
        
        if radius == 0:
            angle = None
        else:
            uc = Unit_Circle(radius)
            angle = uc.xy_to_angle(x=dx, y=dy, unit=unit)
        
        res = {"radius" : radius, "angle" : angle}

        return res

    def other_point_orientation(self, point, unit="deg"):
        """
        >>> p0 = Point(0,0)
        >>> p1 = Point(1,1)
        >>> o =p0.other_point_orientation(p1, "deg")
        >>> o == 45.0
        True
        """
        dic = self.return_position_with_other_point(point=point, unit=unit)
        res = dic["angle"]

        return res

    def other_point_cardinal_orientation(self, point, n_dir=4):
        """
        >>> p0 = Point(0,0)
        >>> p1 = Point(1,0)
        >>> p0.other_point_cardinal_orientation(p1, 4)
        'E'
        >>> p1 = Point(0,1)
        >>> p0.other_point_cardinal_orientation(p1, 4)
        'N'
        """
        unit = "deg"
        angle = self.other_point_orientation(point=point, unit=unit)
        res = cardinal_direction_from_angle(angle, n_dir=n_dir, unit=unit)

        return res

    def is_inside_the_area(self, lons, lats):
        """
        >>> pt = Point(x = 10, y = 10)
        >>> pt.is_inside_the_area((0, 20), (0, 20))
        True
        >>> pt.is_inside_the_area((10, 10), (10, 10))
        True
        >>> pt.is_inside_the_area((10, 11), (10, 11))
        True
        >>> pt.is_inside_the_area((0, 9.9), (0, 9.9))
        False
        >>> pt.is_inside_the_area(None, None)
        True
        >>> pt.is_inside_the_area(None, (0, 9.9))
        False
        >>> pt.is_inside_the_area(None, (10, 10))
        True
        >>> pt.is_inside_the_area((10, 10), None)
        True
        >>> pt.is_inside_the_area((0, 9.9), None)
        False
        """

        if lons is None:
            cond1 = True
        else:
            assert isinstance(lons, (list, tuple)), "lons must be list type"
            assert len(lons) == 2, f"'lons' doit contenir 2 element got {len(lons)}"
            cond1 = self.x >= lons[0] and self.x <= lons[1]

        if lats is None:
            cond2 = True
        else:
            assert isinstance(lats, (list, tuple)), "lats must be list type"
            assert len(lats) == 2, f"'lons' doit contenir 2 element got {len(lats)}"
            cond2 = self.y >= lats[0] and self.y <= lats[1]

        res = cond1 and cond2

        return res
            
    def add_covariate(self, key, value):
        """
        Set a covariate
        >>> p = Point(0,0)
        >>> p.add_covariate("club", "PLA")
        >>> p.club
        'PLA'
        >>> p.add_covariate("club", "GACK")
        Traceback (most recent call last):
                ...
        AssertionError: key club already there : value = PLA

        >>> p.add_covariate("place", "QC")
        >>> p.place
        'QC'
        """
        assert key not in self.__dict__, "key {0} already there : value = {1}".format(key, self.__dict__[key])
        assert key not in ("x", "y", "z"), "key {0} already there : value = {1}".format(key, getattr(self, key))
        setattr(self, key, value)
        
        return None

    def plot(self, text=False, **kwargs):
        """Plot the point"""
        if text:
            
            if "short" in kwargs:
                short = kwargs.pop("short")            
                if short is True:
                    txt = self.txt[:3]
                elif isinstance(short, int):
                    txt = self.txt[:short]
                else:
                    raise ValueError(f"Exp True, False or int, got {short}")
            else:
                txt = self.txt
            
            if "xoffset" in kwargs:
                dx = kwargs.pop("xoffset")
                x = self.x + dx
            else:
                x = self.x
        
        
            if "yoffset" in kwargs:
                dy = kwargs.pop("yoffset")
                y = self.y + dy
            else:
                y = self.y
            
            argdic = {'ha' : 'center', 'va' : 'center', 'fontsize' : 12.}
            argdic.update(kwargs)
            
            res = pyplot.text(x, y, txt, **argdic)
        
        else:
            argdic = dict(marker="o", mec="k", mfc="0.5", mew=2)
            argdic.update(kwargs)
            res = pyplot.plot(self.x, self.y, **argdic)[0]
        
        return res
        

class MultiPoints(shapely.geometry.MultiPoint):
    """ Creation d'un objet contenant une list de point ayant des coordonnee x, y et des valeurs
    Entree:
    - points    :   Une liste ou une tuple de points
    >>> pl = MultiPoints((Point(2, 5, 2.36), Point(1, 4, 2.56)))
    >>> pl.to_df()
         x    y     z
    0  2.0  5.0  2.36
    1  1.0  4.0  2.56
    >>> print(pl)
         x    y     z
    0  2.0  5.0  2.36
    1  1.0  4.0  2.56


    >>> rect = MultiPoints([Point([0, 0, 1, 1][i], [0, 1, 1, 0][i]) for i in range(4)])
    >>> pt = rect.centroid
    >>> pt.x, pt.y
    (0.5, 0.5)
    >>> tri = MultiPoints([Point([0, 3, 0][i], [1, 2, 3][i]) for i in range(3)])
    >>> pt = tri.centroid
    >>> pt.x, pt.y
    (1.0, 2.0)
    
    >>> import scipy.stats
    >>> func = scipy.stats.norm()
    >>> pts = [Point(x=i,y=i*2,z=i * 3) for i in range(10)]
    >>> pts = MultiPoints(pts)
    >>> sorted([i for i in pts.__dict__.keys() if '_' not in i])
    []
    
    >>> pts.to_df()
         x     y     z
    0  0.0   0.0   0.0
    1  1.0   2.0   3.0
    2  2.0   4.0   6.0
    3  3.0   6.0   9.0
    4  4.0   8.0  12.0
    5  5.0  10.0  15.0
    6  6.0  12.0  18.0
    7  7.0  14.0  21.0
    8  8.0  16.0  24.0
    9  9.0  18.0  27.0

    """
    def __init__(self, *arg):
        """
        init object
        """
        if len(arg) == 1:
            points = arg[0]
            assert isinstance(points, Iterable), "an Iterable is requiresd, got {0}".format(type(points))
            assert len(points) > 1, "more than 1 point is required to create points instance, got {0}".format(points)

        elif len(arg) > 1:
            points = arg
        else:
            raise ValueError("must have more than one point got\n{0}".format(arg))
        
        shapely.geometry.MultiPoint.__init__(self, points)
        
        p0 = points[0]
        if isinstance(p0, Point) and p0.txt:            
            self.txts = numpy.array([p.txt for p in points])
        
    def __str__(self):
        """
        Print function
        """
        res = pandas.DataFrame.__str__(self.to_df())
        return res
        
        
    def to_df(self, txts=False):
        """
        DataFrame repr of object
        """
        xs = numpy.array([p.x for p in self.geoms], dtype=float)
        ys = numpy.array([p.y for p in self.geoms], dtype=float)
        if txts:
            zs = self.txts
        else:    
            zs = numpy.array([p.z for p in self.geoms], dtype=float)
        
        
        df = {XKEY : xs, YKEY : ys, ZKEY : zs}
        res = pandas.DataFrame(df, columns=(XKEY, YKEY, ZKEY))
        return res
        
    def points(self, txts=False):
        """
        Return a list of points
        """
        df = self.to_df(txts=txts)
        res = [Point(**df.loc[iind]) for iind in df.index]
        return res

    def use_projection(self, proj_obj, **kwargs):
        """
        >>> pts = MultiPoints([Point(x=i, y=i*2, z=i**2.) for i in range(10)])
        >>> pts.to_df()
             x     y     z
        0  0.0   0.0   0.0
        1  1.0   2.0   1.0
        2  2.0   4.0   4.0
        3  3.0   6.0   9.0
        4  4.0   8.0  16.0
        5  5.0  10.0  25.0
        6  6.0  12.0  36.0
        7  7.0  14.0  49.0
        8  8.0  16.0  64.0
        9  9.0  18.0  81.0


        >>> bmap=Proj(llcrnrlon=0, llcrnrlat=0, urcrnrlon=1, urcrnrlat=1, proj='merc', lon_0=0, lat_0=0,ellps = 'WGS84')
        >>> npts = pts.use_projection(bmap)
        >>> df = npts.to_df()
        
        """
        df = self.to_df()
        xs = df[XKEY].values
        ys = df[YKEY].values
        
        xyproj = proj_obj(xs, ys, **kwargs)
        xsproj, ysproj = xyproj
        
        df[XKEY] = xsproj
        df[YKEY] = ysproj
        
        nwpts = [Point(**df.loc[iind]) for iind in df.index]
        res = MultiPoints(nwpts)
        
        return res

    def plot(self, **kwargs):
        """
        #~ >>> p_list = [Point(x=scipy.stats.norm(loc=0, scale=1).rvs(1)[0], y=scipy.stats.norm(loc=6, scale=1).rvs(1)[0], z=scipy.stats.norm().rvs(1)[0]) for i in range(50)]
        #~ >>> p_l = MultiPoints(p_list)
        #~ >>> f=pyplot.figure()
        #~ >>> r=p_l.plot()
        #~ >>> r=p_l.plot(marker = "s")
        #~ >>> f.show()
        #~ >>> pyplot.close()
        """
        print("DEPRECATED, use PtSeries")
        

        df = self.to_df()

        res = pyplot.plot(df[XKEY], df[YKEY], **kwargs)[0]
        return res


    def scatter(self, **kwarg):
        """
        #~ >>> from functions import *
        #~ >>> p_list = [Point(x=scipy.stats.norm(loc=0, scale=1).rvs(1)[0], y=scipy.stats.norm(loc=6, scale=1).rvs(1)[0], z=scipy.stats.norm().rvs(1)[0]) for i in range(10)]
        #~ >>> p_l = MultiPoints(p_list)
        #~ >>> lim = normalize_list(p_l.to_df()['z'], factor = 1)
        #~ >>> vn, vx = lim
        #~ >>> bds = numpy.arange(vn, vx, 0.5)
        
        #~ >>> from functions import *
        #~ >>> cmnm = 'viridis'
        #~ >>> fig = pyplot.figure()
        #~ >>> axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))
        #~ >>> ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))

        #~ >>> nbds = len(bds)
        #~ >>> ncol = nbds - 1
        #~ >>> cm = get_colormap(name = cmnm, ncol = ncol, extend = True)
        #~ >>> nrm = matplotlib.colors.BoundaryNorm(bds, ncol)
        #~ >>> pc=p_l.scatter(cmap = cm, norm = nrm)
        #~ >>> cb = pyplot.colorbar(cax = axcb, extend = "both")
        #~ >>> f = pyplot.figure()
        #~ >>> axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))
        #~ >>> ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))
        #~ >>> cb = plot_colorbar(bds, cax = axcb)
        #~ >>> lc = p_l.scatter(colorbar =cb)
        #~ >>> show_and_close()

        """
        print("DEPRECATED, use PtSeries")
        if "colorbar" in kwarg:
            #~ assert len(kwarg) == 1
            cb = kwarg.pop("colorbar")
            kwarg.update(dict(cmap=cb.cmap, norm=cb.norm))
        
        argdic = {"alpha" : 1, "s" : 100, "lw" : 1}
        argdic.update(kwarg)
        
        df = self.to_df()        
        res = pyplot.scatter(x=df[XKEY], y=df[YKEY], c=df[ZKEY], **argdic)
        return res

    def annotate_plot(self, **kwarg):
        """
        #~ >>> f=pyplot.figure()
        #~ >>> ts= example_points(5, texts=True).annotate_plot()
        #~ >>> f.show()
        #~ >>> pyplot.close()
        """
        print("DEPRECATED, use PtSeries")
        #~ print("broken use loop on points")
        res = [p.annotate_plot(**kwarg) for p in self.points(txts=True)]
        return res


    

##======================================================================================================================##
##                LINE                                                                                                  ##
##======================================================================================================================##

class Line(shapely.geometry.LineString):
    """ Class doc 
    >>> import scipy.stats
    >>> f = scipy.stats.norm()
    >>> pts = [Point(x=0, y=i) for i in range(10)]
    >>> l = Line(pts)
    >>> l.length
    9.0
    >>> p = l.plot()
    >>> sorted([i for i in l.__dict__.keys() if '_' not in i])
    []
    >>> pyplot.gcf().show()
    >>> pyplot.close()
    >>> print(l)
         x    y
    0  0.0  0.0
    1  0.0  1.0
    2  0.0  2.0
    3  0.0  3.0
    4  0.0  4.0
    5  0.0  5.0
    6  0.0  6.0
    7  0.0  7.0
    8  0.0  8.0
    9  0.0  9.0
    >>> l.to_df()
         x    y
    0  0.0  0.0
    1  0.0  1.0
    2  0.0  2.0
    3  0.0  3.0
    4  0.0  4.0
    5  0.0  5.0
    6  0.0  6.0
    7  0.0  7.0
    8  0.0  8.0
    9  0.0  9.0
    
    >>> l.lims()
           x    y
    min  0.0  0.0
    max  0.0  9.0
    
    >>> bmap=Proj(llcrnrlon=-0.1,llcrnrlat=1.1,urcrnrlon=0,urcrnrlat=1,proj='cass',lon_0=0.5,lat_0=0.5,ellps='WGS84')
    >>> pl = l.use_projection(bmap)
    >>> pl.to_df().round(0)
             x         y
    0 -55660.0  -55287.0
    1 -55651.0   55291.0
    2 -55626.0  165871.0
    3 -55584.0  276451.0
    4 -55525.0  387034.0
    5 -55449.0  497619.0
    6 -55357.0  608208.0
    7 -55248.0  718801.0
    8 -55122.0  829398.0
    9 -54979.0  940001.0

    """
    
    def __init__(self, points):
        """ Class initialiser """
        shapely.geometry.LineString.__init__(self, points)
        
    def __str__(self):
        """
        Print function
        """
        res = pandas.DataFrame.__str__(self.to_df())
        return res
        
        
    def to_df(self):
        """
        DataFrame representation of the object
        """
        xy = numpy.array(self.coords.xy, dtype=float)
        x, y = xy

        df = {XKEY : x, YKEY : y}
        res = pandas.DataFrame(df, columns=(XKEY, YKEY))
        return res


    def plot(self, **kwarg):
        """
        Plot line
        """
        argdic = DEFLINE.copy()
        argdic.update(kwarg)
        
        df = self.to_df()
        xs = df[XKEY]
        ys = df[YKEY]
        res = pyplot.plot(xs, ys, **argdic)[0]
        return res

    def use_projection(self, proj_obj, **kwargs):
        """
        Project the object
        """
        
        df = self.to_df()
        xs = df[XKEY].values
        ys = df[YKEY].values
        
        xyproj = proj_obj(xs, ys, **kwargs)
        xsproj, ysproj = xyproj
        
        df[XKEY] = xsproj
        df[YKEY] = ysproj
        
        nwpts = [Point(**df.loc[iind]) for iind in df.index]
        res = Line(nwpts)
        
        return res
        
    def lims(self, **kwargs):
        """
        Return limits
        """
        df = self.to_df()
        res = XYlims(df=df, **kwargs)
        return res

##======================================================================================================================##
##                PLOYGON                                                                                               ##
##======================================================================================================================##

class Ring(shapely.geometry.LinearRing):
    """ Class doc
    >>> pts = [Point(x=i, y=j) for i, j in zip([0,0, 1], [0,1,0])]
    >>> l = Ring(pts)
    
    >>> l.to_df()
         x    y
    0  0.0  0.0
    1  0.0  1.0
    2  1.0  0.0
    3  0.0  0.0
    >>> print(l)
         x    y
    0  0.0  0.0
    1  0.0  1.0
    2  1.0  0.0
    3  0.0  0.0
    
    >>> l.lims()
           x    y
    min  0.0  0.0
    max  1.0  1.0
    
    >>> f = pyplot.figure()
    >>> p = l.plot()
    >>> f.show()
    >>> pyplot.close()
    
    >>> bmap=Proj(llcrnrlon=-0.1,llcrnrlat=1.1,urcrnrlon=0,urcrnrlat=1,proj='cass',lon_0=0.5,lat_0=0.5,ellps='WGS84')
    >>> pl = l.use_projection(bmap)
    >>> pl.to_df().round(0)
             x        y
    0 -55660.0 -55287.0
    1 -55651.0  55291.0
    2  55660.0 -55287.0
    3 -55660.0 -55287.0
    """
    
    def __init__(self, points):
        """ Class initialiser """
        #~ MultiPoints.__init__(self, points)
        if isinstance(points, (list, tuple)):
            points = shapely.geometry.MultiPoint(points)
        
        shapely.geometry.LinearRing.__init__(self, points)
        

    def __str__(self):
        """
        Print function
        """
        res = pandas.DataFrame.__str__(self.to_df())
        return res
        
    def to_df(self):
        """
        DateFrame repr of object
        """
        xy = numpy.array(self.coords.xy, dtype=float)
        x, y = xy
        df = {XKEY : x, YKEY : y}
        res = pandas.DataFrame(df, columns=(XKEY, YKEY))
        return res

    def new_resolution(self, nps):
        """
        >>> ring = Ring([Point([0, 0, 1, 1][i], [0, 1, 1, 0][i]) for i in range(4)])
        >>> ring.to_df()
             x    y
        0  0.0  0.0
        1  0.0  1.0
        2  1.0  1.0
        3  1.0  0.0
        4  0.0  0.0
        >>> nring = ring.new_resolution(2)
        >>> nring.to_df()
             x    y
        0  0.0  0.0
        1  0.0  0.5
        2  0.0  1.0
        3  0.5  1.0
        4  1.0  1.0
        5  1.0  0.5
        6  1.0  0.0
        7  0.5  0.0
        8  0.0  0.0
        """
        df = self.to_df()

        dfs = []

        for iii in range(1, df.index.size):
            indf = df.iloc[iii-1]
            ixdf = df.iloc[iii]
            nx = numpy.linspace(indf[XKEY], ixdf[XKEY], nps + 1, endpoint=True)
            ny = numpy.linspace(indf[YKEY], ixdf[YKEY], nps + 1, endpoint=True)
            #keep all but last
            nx = nx[:-1]
            ny = ny[:-1]
            ndf = pandas.DataFrame({XKEY : nx, YKEY: ny}, columns=(XKEY, YKEY))
            dfs.append(ndf)
    
        new_df = pandas.concat(dfs)

        res = Ring(new_df.values)

        return res

    def to_path(self):
        """
        Ring to path for ploting
        >>> path = Ring([Point([0, 0, 1, 1][i], [0, 1, 1, 0][i]) for i in range(4)]).to_path
        """
        vers = self.to_df()
        codes = [Path.MOVETO] +  [Path.LINETO] * (len(vers) - 1)
        res = Path(vertices=vers, codes=codes)
        return res

    def plot(self, **kwarg):
        """
        Plot ring
        """
        argdic = DEFLINE.copy()
        argdic.update(kwarg)
        
        df = self.to_df()
        xs = df[XKEY]
        ys = df[YKEY]
        res = pyplot.plot(xs, ys, **argdic)[0]
        return res
        
    def use_projection(self, proj_obj, **kwargs):
        """
        Project the ring
        """
        df = self.to_df()
        xs = df[XKEY].values
        ys = df[YKEY].values
        
        xyproj = proj_obj(xs, ys, **kwargs)
        xsproj, ysproj = xyproj
        
        df[XKEY] = xsproj
        df[YKEY] = ysproj
        
        nwpts = [Point(**df.loc[iind]) for iind in df.index]
        res = Ring(nwpts)
        
        return res
    
    def lims(self, **kwargs):
        """
        Return limits
        """
        df = self.to_df()
        res = XYlims(df=df, **kwargs)
        return res

class Polygon(shapely.geometry.Polygon):
    """
    >>> poly = example_poly(0)
    >>> p = shapely.geometry.Polygon(poly)
    >>> poly = example_poly(1)
    >>> p = shapely.geometry.Polygon(poly)
    >>> [i for i in p.__dict__.keys() if '_' not in i]
    []
    >>> poly.to_df()
         x    y
    0  0.0  0.0
    1  0.0  1.0
    2  1.0  1.0
    3  1.0  0.0
    4  0.0  0.0
    >>> print(poly)
         x    y
    0  0.0  0.0
    1  0.0  1.0
    2  1.0  1.0
    3  1.0  0.0
    4  0.0  0.0
    
    >>> nrpoly = example_poly(False)
    >>> p1 = Point(nrpoly.centroid)
    >>> p2 = Point(nrpoly.representative_point())
    >>> f = pyplot.figure()

    >>> l=nrpoly.plot(adjust_lim = True)
    >>> p1.to_df()
         x    y    z
    0  1.7  2.0  0.0
    >>> p2.to_df()
         x    y    z
    0  0.5  2.0  0.0

    
    >>> poly = Polygon([Point([0, 0, 2, 1][i], [0, 1, 1, 0][i]) for i in range(4)])
    >>> po= Point(0, 0)

    >>> poly.contains(po)
    False
    >>> poly.touches(po)
    True
    >>> poly.contains(Point(0.5, 0.5))
    True
    >>> poly.contains(Point(0.5, 1.01))
    False

    >>> bmap=Proj(llcrnrlon=-0.1,llcrnrlat=1.1,urcrnrlon=0,urcrnrlat=1,proj='cass',lon_0=0.5,lat_0=0.5,ellps='WGS84')
    >>> poly = Polygon([Point([0, 0, 2, 1][i], [0, 1, 1, 0][i]) for i in range(4)])
    >>> projpoly = poly.use_projection(bmap)
    >>> projpoly.to_df().round()
              x        y
    0  -55660.0 -55287.0
    1  -55651.0  55291.0
    2  166954.0  55325.0
    3   55660.0 -55287.0
    4  -55660.0 -55287.0

    """
    def __init__(self, shell=None, holes=None):
        """ Object init """
        if isinstance(shell, (list, tuple)):
            shell = shapely.geometry.MultiPoint(shell)

        shapely.geometry.Polygon.__init__(self, shell=shell, holes=holes) #shell = coque
        #~ shapely.geometry.Polygon.__init__(self, **kwarg) #shell = coque
    
    
    def __str__(self):
        """
        Print function
        """
        res = pandas.DataFrame.__str__(self.to_df())
        return res
        
    def to_df(self):
        """
        DataFrame representation
        """
        ext = self.exterior
        ext = Ring(ext)
        res = ext.to_df()
        return res
    
    def to_path(self):
        """
        To path for ploting
        >>> path = Polygon([Point([0, 0, 1, 1][i], [0, 1, 1, 0][i]) for i in range(4)]).to_path
        """
        extring = Ring(self.exterior)
        extpath = extring.to_path()

        if not self.interiors:
            res = extpath
        else:
            codes = extpath.codes
            vers = extpath.vertices
            for i_int in self.interiors:
                iring = Ring(i_int)
                path = iring.to_path()
                codes = numpy.append(codes, path.codes)
                vers = numpy.concatenate([vers, path.vertices])
                        
            res = Path(vertices=vers, codes=codes)
            
        return res

    def plot(self, adjust_lim=False, **kwarg):
        """
        >>> pl=Polygon([Point([0, 0, 5, 5][i], [0, 1, 1, 0][i]) for i in range(4)])
        >>> f=pyplot.figure()
        >>> l=pl.plot()
        >>> f.show()
        >>> pyplot.close()
        """
        argdic = {'ec' : 'k', 'fc' : "0.5", "ax" : pyplot.gcf().gca(), 'alpha':1, 'zorder':1}
        argdic.update(kwarg)
        
        ax = argdic.pop("ax")
        
        path = self.to_path()
        patch = PathPatch(path, **argdic)
        
        res = ax.add_patch(patch)   
        
        if adjust_lim:
            f = 0.05
            lims = self.lims(factor=f)
            xlim = lims[XKEY]
            ylim = lims[YKEY]
            pyplot.xlim(xlim)
            pyplot.ylim(ylim)
        
        return res
        
    def lims(self, **kwargs):
        """
        Return limits of polygon, multiply by a factor if given
        >>> pl=Polygon([Point([0, 0, 5, 5][i], [0, 1, 1, 0][i]) for i in range(4)])
        >>> pl.lims()
               x    y
        min  0.0  0.0
        max  5.0  1.0
        >>> pl.lims(factor = 5)
             x  y
        min  0  0
        max  5  5
        >>> pl.lims(factor = 10)
              x   y
        min   0   0
        max  10  10


        """
        df = self.to_df()
        res = XYlims(df=df, **kwargs)
        return res

    def use_projection(self, proj_obj, **kwargs):
        """
        Projet the obj
        """
        if not self.is_valid:
            self1 = self.buffer(0)                                                                                                                                                                                                    
            assert self1.equals(self)
            extring = Ring(self1.exterior)
        else:
            extring = Ring(self.exterior)
            
        new_ext = extring.use_projection(proj_obj=proj_obj, **kwargs)

        if not self.interiors:
            new_ints = None
        else:
            new_ints = [Ring(i_int) for i_int in self.interiors]
            new_ints = [iring.use_projection(proj_obj=proj_obj, **kwargs) for iring in new_ints]

        res = Polygon(new_ext, holes=new_ints)
        
        if not res.is_valid:
            res = res1.buffer(0)
            res1 = Polygon(res1)
            assert res1.equals(res)
            res = res1
            assert res.is_valid, 'invalid polygon'
        
        return res
   



##======================================================================================================================##
##                PREDEFINED FORM                                                                                       ##
##======================================================================================================================##


class Unit_Circle(Polygon):
    """ Class doc
    >>> p= Unit_Circle().plot()
    >>> pyplot.gcf().show()
    >>> pyplot.close()
    >>> uc = Unit_Circle(1)
    >>> "{0:.5f}".format(uc.area)
    '3.14159'
    >>> "{0:.5f}".format(math.pi)
    '3.14159'
    
    """


    def __init__(self, radius=1):
        """ Class initialiser """
        assert isinstance(radius, (int, float)), 'Expected {0} got {1.__class__} : {1}'.format("numb", radius)
        self.radius = float(radius)
        
        poly = Point(0, 0).buffer(self.radius, resolution=1000)
        Polygon.__init__(self, poly)
            

    def _x_to_radians(self, x):
        """
        Return raridans from y
        """
        x = x / float(self.radius)
        rad = math.acos(x)
        #~ rad = round(rad, 5)
        res = (rad, 2*math.pi - rad)

        return res

    def _y_to_radians(self, y):
        """
        Return raridans from y
        """
        y = y / float(self.radius)
        rad = math.asin(y)
        #~ rad = round(rad, 5)
        if rad > 0:
            res = (rad, math.pi - rad)
        else:
            rad = - rad
            res = (2*math.pi - rad, math.pi + rad)

        return res
            
            
    def xy_to_angle(self, x, y, unit="deg"):
        """
        
        >>> Unit_Circle(1).xy_to_angle(0,1)
        90.0
        >>> Unit_Circle(1).xy_to_angle(-1,0)
        180.0
        >>> Unit_Circle(1).xy_to_angle(0,-1)
        270.0
        >>> Unit_Circle(1).xy_to_angle(1,0)
        360.0
        """
        xrad = self._x_to_radians(x)
        yrad = self._y_to_radians(y)

        
        #~ print(xrad, yrad

        res = [x for x in xrad for y in yrad if abs(x - y) < 1e-8]
        
        assert res, "pb : x and y are not in the unit circle"

        
        res = list(set(res))
        
        if len(res) != 1:
            print("pb : two possibilities : take the first : {0}".format(res))
        
        res = res[0]



        #~ print(res
                
        if unit in DEGREES_NAMES:
            res = math.degrees(res)
            res = round(res, 8)
        
        return res

    def angle_to_point(self, angle, unit="deg", point_obj=False):
        """
        >>> p = Unit_Circle().angle_to_point(0, unit = "deg", point_obj = True)
        >>> p.__class__ == Point
        True
        >>> p.x, p.y
        (1.0, 0.0)
        """
                        
        if unit in DEGREES_NAMES:
            angle = math.radians(angle)
                
        x = math.cos(angle)
        y = math.sin(angle)
        
        x = x * self.radius
        y = y * self.radius
        
        if point_obj:
            res = Point(x, y)
        else:
            res = (x, y)
        #~ print(res
        
        return res
            

class Rectangle(Polygon):
    """ Class doc
    >>> fig = pyplot.figure()
    >>> p=Rectangle([Point(3,1), Point(2,5)]).plot(fc = "red")
    >>> p=Rectangle(Point(-3,1), Point(-2,-5)).plot(fc = "turquoise")
    >>> p=Rectangle(xlim = (0,1), ylim= (1,2)).plot(fc = "darkred")
    >>> l=pyplot.xlim(-10, 10)
    >>> l=pyplot.ylim(-10, 10)
    >>> pyplot.grid(1)
    >>> fig.show()
    
    >>> points= Rectangle(xlim = (0, 1), ylim = (0, 1))
    >>> points.__class__ == Rectangle
    True
    >>> points.to_df()
         x    y
    0  0.0  0.0
    1  0.0  1.0
    2  1.0  1.0
    3  1.0  0.0
    4  0.0  0.0
    
    """
    
    def __init__(self, *arg, **kwarg):
        """ Class initialiser """
        #~ print(arg, kwarg)
        
        if len(arg) == 1:
            arg = arg[0]
        
        if len(arg) == 2:
            pt1, pt3 = arg
            pt2 = Point(x=pt1.x, y=pt3.y)
            pt4 = Point(x=pt3.x, y=pt1.y)

            recpts = [pt1, pt2, pt3, pt4]
            
        elif "xlim" in kwarg and "ylim" in kwarg:
            xlim = kwarg.pop("xlim")
            ylim = kwarg.pop("ylim")
            assert isinstance(xlim, (tuple, list, numpy.ndarray)), "pb exp list, got {0}".format(xlim)
            assert isinstance(ylim, (tuple, list, numpy.ndarray)), "pb exp list, got {0}".format(ylim)
            assert len(xlim) == 2 and len(ylim) == 2, "pb dim"
            
            xs = [0, 0, 1, 1]#x : 0 0 1 1
            ys = [0, 1, 1, 0]#y : 0 1 1 0
            
            recpts = [Point(x=xlim[ix], y=ylim[iy]) for ix, iy in zip(xs, ys)]

        else:
            raise ValueError(f"dont know what to do with:\nargs={arg}\nkwarg={kwarg}")
        
        assert not kwarg, f"there is remaining kwarg. Dont know what to do\n{kwarg}"
        
        Polygon.__init__(self, recpts)




##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    parser.add_argument('--pickle', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    examples                   #
    #+++++++++++++++++++++++++++++++#
    if opts.examples:
        

        pyplot.close("all")
        #~ b = utm_proj((-20, 15), (4, 16))


        m_pts = []
        for xm in numpy.arange(-10, 10, 1):
            for ym in numpy.arange(5, 15, 1):
                m_pt = Point(x=xm, y=ym, z=str(xm*ym))
                m_pts.append(m_pt)

        m_pts = MultiPoints(m_pts)


        fig = pyplot.figure()
        triangle = Polygon([Point(xm, ym) for xm, ym in zip([0, 0.5, 1], [0, 1, 0])])
        triangle.plot()
        
        fig.show()
            
        
        #+++++++++++++++++++++++++++++++#
        #        UNIT CIRCLE            #
        #+++++++++++++++++++++++++++++++#
        
        p0m = Point(0, 0)
        ps = [Point(xm, ym) for xm, ym in zip([1, -1, -1, 1], [1, 1, -1, -1])]
        p1 = ps[0]
        
        d = distance_2pts(p0m, p1)
        
        uc_m = Unit_Circle(d)
        fig=pyplot.figure()
        uc_m.plot(True)
        fig.show()
        
        for ip in ps:
            #~ a = p0m.other_point_orientation(p, False)
            a = p0m.other_point_cardinal_orientation(ip, n_dir=8)
            ip.value = str(a)
            ip.plot(text=True)
        
        ps = [Point(xm, ym) for xm, ym in zip([1, 0, -1, 0], [0, 1, 0, -1])]
        p1 = ps[0]
        
        d = distance_2pts(p0m, p1)
        
        uc_m = Unit_Circle(d)
        uc_m.plot()
        
        for ip in ps:
            #~ a = p0m.other_point_orientation(p, False)
            a = p0m.other_point_cardinal_orientation(ip, n_dir=8)

            ip.value = str(a)
            ip.plot(text=True)
        
        
        
        

        #~ p = Proj

        #~ from shapely import wkt
        #~ from shapely import ops

        point1 = shapely.wkt.loads('POINT (4 5)')
        mp = Point(point1)
        
        fig = pyplot.figure()
        #~ mp.plot(mfc="r", label="pt")

        multipt = shapely.wkt.loads('MULTIPOINT ((2 2), (3 3), (4 4))')
        mpts = MultiPoints(multipt)
        

        # ~ mpts.plot(mfc="b", label="pts")

        mline = shapely.wkt.loads('LINESTRING (3 1, 4 4, 5 5, 5 6)')
        ml = Line(mline)
        

        ml.plot(color="g")

        poly1 = shapely.wkt.loads('POLYGON ((1 2, 2 5, 6 5, 7 2, 5 1, 1 2))')
        mpoly = Polygon(poly1)
        

        mpoly.plot(fc="b", ec="b")



        poly2 = shapely.wkt.loads('POLYGON ((1 2, 2 5, 6 5, 7 2, 5 1, 1 2), (3 4, 2 2, 5 2, 6 4, 3 4))')
        mpoly = Polygon(poly2)
        

        mpoly.plot(fc="r", ec="r")
        
        fig.show()

        fig = pyplot.figure()
        
        mp = Point(0, 0)
        #~ mp.plot(mfc="w", mec="k")
        b = mp.buffer(2)
        b = Polygon(b)
        b.plot(ec="k", fc="0.5")
        pyplot.xlim(-2, 2)
        pyplot.ylim(-2, 2)
        
        fig.show()
        
        fig = pyplot.figure()
        
        dxm = 0.25
        dym = 0.25
        xlimm = numpy.array([-1, 1])
        ylimm = numpy.array([-1, 1])
        square = Rectangle(xlim=xlimm, ylim=ylimm)
        center = Point(square.centroid)
        
        square2 = Polygon(square)
        for xm, ym in zip([0.5, -0.5, -0.5, 0.5], [0.5]*2+[-0.5]*2):
            s = Rectangle(xlim=[xm-dxm, xm+dxm], ylim=[ym-dym, ym+dym])
            
            square2 = square2.difference(s)
                
        square2 = Polygon(square2)
        
        square2.plot()
        center = Point(center)
        #~ center.annotate_plot()
        
        pyplot.xlim(1.1 * xlimm)
        pyplot.ylim(1.1 * ylimm)
        
        fig.show()
        
        fig = pyplot.figure()
        
        radm = 0.55
        
        circles = []
        
        for xm, ym in zip([0.5, -0.5, -0.5, 0.5], [0.5]*2+[-0.5]*2):
            mpt = Point(xm, ym)
            c = mpt.buffer(radm)
            c = Polygon(c)
            circles.append(c)

        new = reduce_polygons(circles)
        new = Polygon(new)
        
        new.plot()
        
        pyplot.xlim(1.1 * xlimm)
        pyplot.ylim(1.1 * ylimm)

        fig.show()

        bmap = Proj(llcrnrlon=-0.1, llcrnrlat=1.1, urcrnrlon=0, urcrnrlat=1, proj='cass', lon_0=0.5, lat_0=0.5, ellps='WGS84')

    #+++++++++++++++++++++++++++++++#
    #    TEST PICKLE                #
    #+++++++++++++++++++++++++++++++#
    
    
    if opts.pickle:
        
        
        pp = Polygon(shell=[Point(xm, ym) for xm, ym in zip([0, 0.5, 1], [0, 1, 0])])
        fpt = os.path.join(os.getenv('HOME'), "poly.pic") #"/home/panthou/Bureau/poly.pic"
        print("save")
        with open(fpt, "wb") as picw:
            pickle.dump(obj=pp, file=picw)
        
        print("load")
        with open(fpt, "rb") as picr:
            pl = pickle.load(file=picr)
            
        os.remove(fpt)
    
        print("test")
        assert pp == pl
        print("ok")
        
        
        pp = shapely.geometry.Point(1, 5, 6)
        #~ pp = Point(x=1, y = 5, z = 6)
        fpt = os.path.join(os.getenv('HOME'), "point.pic") #"/home/panthou/Bureau/poly.pic"
        print("save")
        with open(fpt, "wb") as picw:
            pickle.dump(obj=pp, file=picw)
        
        print("load")
        with open(fpt, "rb") as picr:
            pl = pickle.load(file=picr)
    
        os.remove(fpt)
        print("test")
        assert pp == pl
        print("ok")
