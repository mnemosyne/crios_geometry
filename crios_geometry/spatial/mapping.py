"""
Module : to transform and use different projection
"""



##======================================================================================================================##
##                                PACKAGE                                                                               ##
##======================================================================================================================##

import math
import numpy
import pandas
from collections.abc import Iterable
from pyproj import Proj

from crios_geometry.cst import DEGREES_NAMES

##======================================================================================================================##
##                                CST                                                                                   ##
##======================================================================================================================##

PROJ_TYPE = ['Proj', 'Basemap']

WKEY = "W"
EKEY = "E"
EWKEY = "EW"

OKEYS = (WKEY, EKEY, EWKEY)

CARDINAL_POINTS = {0 : "E", 90 : "N", 180: "W", 270 : "S", 360: "E"}
CARDINAL_ORDERS = {"N" : 0, "S" : 0, "W" : 1, "E" : 1}


##======================================================================================================================##
##                                PROJ FUNCTIONS                                                                        ##
##======================================================================================================================##

def cardinals_points(n_dir):
    """
    >>> cardinals_points(2)
    Traceback (most recent call last):
            ...
    AssertionError: pb entree demande multiple of 4 got <class 'int'> : 2
    >>> d=cardinals_points(4)
    >>> for deg in sorted(d.keys()): print((d[deg], deg))
    ('E', 0)
    ('N', 90)
    ('W', 180)
    ('S', 270)
    ('E', 360)
    >>> d=cardinals_points(8)
    >>> for deg in sorted(d.keys()): print((d[deg], deg))
    ('E', 0.0)
    ('NE', 45.0)
    ('N', 90.0)
    ('NW', 135.0)
    ('W', 180.0)
    ('SW', 225.0)
    ('S', 270.0)
    ('SE', 315.0)
    ('E', 360.0)
    """
    assert isinstance(n_dir, int), 'pb entree demande {0} got {1.__class__} : {1}'.format("numb", n_dir)
    assert n_dir % 4 == 0, 'pb entree demande {0} got {1.__class__} : {1}'.format("multiple of 4", n_dir)
    
    times = n_dir // 4

    points = range(0, 361, 90)
    
    res = {}
    
    if times == 1:
        for p in points:
            res[p] = CARDINAL_POINTS[p]
    else:
        mixes = []
        for i in points:
            toadd = [i] * times
            mixes += toadd

        res = {}
        
        
        starts = range(len(mixes) - times + 1)
        for ind in starts:
            angles = mixes[ind:ind + times]
            
            car = [CARDINAL_POINTS[a] for a in angles]
            key = numpy.mean(angles)
            
            name = sort_cardinal_name(car)
            
    
            res[key] = name

    return res

def cardinal_direction_from_angle(angle, n_dir=4, unit="deg"):
    """
    >>> cardinal_direction_from_angle(0, 4)
    'E'
    >>> cardinal_direction_from_angle(90, 4)
    'N'
    >>> cardinal_direction_from_angle(15, 8)
    'E'
    >>> cardinal_direction_from_angle(45, 8)
    'NE'
    >>> cardinal_direction_from_angle(75, 8)
    'N'
    """
    assert isinstance(angle, (float, int)), 'pb entree demande {0} got {1.__class__} : {1}'.format("numb", angle)
    assert 0 <= angle <= 360, 'pb entree demande {0} got {1.__class__} : {1}'.format("> 0", angle)
    if unit not in DEGREES_NAMES:
        angle = math.degrees(angle)
    
    car_pts = cardinals_points(n_dir=n_dir)
    angles = numpy.sort(list(car_pts.keys()))
    difs = angles - angle
    difs = numpy.abs(difs)
    #~ print(difs
    ind = numpy.argmin(difs)
    best = angles[ind]
    
    res = car_pts[best]  
    
    return res

def sort_cardinal_name(name):
    """
    >>> sort_cardinal_name('WN')
    'NW'
    """
    if isinstance(name, str):
        name = [i for i in name]
    elif isinstance(name, (list, tuple)):
        pass
    else:
        assert 0, 'pb entree demande {0} got {1.__class__} : {1}'.format("str", name)
    
    shorts = list(set(name))
    if len(shorts) == 1:
        name = shorts
                            
    
    orders = [CARDINAL_ORDERS[i] for i in name]
    #~ print(orders
    indexes = numpy.argsort(orders)
    #~ print(indexes
    res = [name[i] for i in indexes]
    res = "".join(res)
    
    return res


def is_projobj(obj):
    """
    Test if the object is a projection
    >>> is_projobj(Proj(proj='utm',zone=10,ellps='WGS84'))
    True
    """
    objtype = str(obj.__class__)
    isprojs = [iproj in objtype for iproj in PROJ_TYPE]
    res = any(isprojs)
    return res
        
def cmp_projobjs(*arg):
    """
    Compare proj objects given
    >>> p=Proj(llcrnrlon=0, llcrnrlat=0, urcrnrlon=1, urcrnrlat=1, proj='merc', lon_0=0, lat_0=0,ellps = 'WGS84')

    >>> p1=Proj(llcrnrlon=0, llcrnrlat=0, urcrnrlon=1, urcrnrlat=1, proj='merc', lon_0=0, lat_0=0,ellps = 'WGS84')
    >>> p2 = Proj(llcrnrlon=0, llcrnrlat=10, urcrnrlon=10, urcrnrlat=1, proj='merc', lon_0=5, lat_0=5,ellps = 'WGS84')
    >>> cmp_projobjs(p1,p)
    True
    >>> cmp_projobjs([p1,p])
    True
    >>> cmp_projobjs(p,p2)
    False
    >>> cmp_projobjs([p,p1,p2])
    False
    """
    
    if len(arg) == 1:
        assert isinstance(arg, (list, tuple)), "pb input : {0}".format(arg)
        arg = arg[0]

    assert all([is_projobj(i)] for i in arg), "pb input {0}".format(arg)
    projtyp = [str(i.__class__) for i in arg]
    projtyp = list(set(projtyp))
    
    if len(projtyp) == 1:
        projtyp = projtyp[0]
        assert isinstance(projtyp, str)
    else:
        raise ValueError("must be same proj class, got : {0}".format(projtyp))
    
    if "Proj" in projtyp:
        propars = [i.srs for i in arg]
    elif "Basemap" in projtyp:
        propars = [i.projparams for i in arg]
    else:
        assert 0, "{0} is not a valid proj typ".format(projtyp)
    
    i0 = propars[0]
    res = all([i == i0 for i in propars])
    
    return res
        
        

def proj_with_full_lonlat_params(projection, lons, lats, ellps='WGS84'):
    """
    >>> p=proj_with_full_lonlat_params(lons = (0,1), lats=(-90, 90), projection = 'tmerc')
    >>> type(p) == Proj
    True
    """
    assert isinstance(lons, (list, tuple)) and len(lons) == 2, 'pb input {0}'.format(lons)
    assert isinstance(lats, (list, tuple)) and len(lats) == 2, 'pb input {0}'.format(lats)

    lon1 = lons[0]
    lon2 = lons[-1]
    lat1 = lats[0]
    lat2 = lats[-1]
    lonm = numpy.mean((lon1, lon2))
    latm = numpy.mean((lat1, lat2))

    res = Proj(proj=projection, llcrnrlon=lon1, llcrnrlat=lat1, urcrnrlon=lon2, urcrnrlat=lat2, lon_0=lonm, lat_0=latm, ellps=ellps)
    
    return res

def utm_proj(lons=(-1, 1), lats=(-1, 1)):
    """
    >>> map=utm_proj()
    >>> type(map) == Proj
    True
    """
    projec = 'tmerc'
    res = proj_with_full_lonlat_params(projection=projec, lons=lons, lats=lats)
    return res

def world_bmap(proj, orientation="EW"):
    """
    Worl projection
    >>> res = world_bmap("mill")
    >>> res.__class__ == Proj
    True
    """
    if orientation == "EW":
        lons = (-180, 180)
        lon0 = 0

    else:
        lons = (0, 360)
        lon0 = 180


    if proj in ("cyl", "mill"):
        res = proj_with_full_lonlat_params(lons=lons, lats=(-90, 90), projection=proj)
    elif proj in ("moll", "robin"):
        res = Proj(proj=proj, lat_0=0, lon_0=lon0)

    else:
        print("warn : output not guaranted")
        try:
            res = proj_with_full_lonlat_params(lons=lons, lats=(-90, 90), projection=proj)
        except:
            print("utm basemap not availble")
            try:
                res = Proj(proj=proj, lat_0=0, lon_0=lon0)
            except:
                assert 0, "proj not available {0}".format(proj)

    return res




def coords2slice(coords):
    """
    Function doc
    >>> coords2slice(None)
    slice(None, None, None)
    >>> coords2slice((10,15))
    slice(10, 15, None)
    >>> coords2slice((10,15,1))
    slice(10, 15, 1)
    >>> coords2slice(slice(10,15))
    slice(10, 15, None)
    >>> coords2slice('nimp')
    Traceback (most recent call last):
    ValueError: Not implemented for coords=nimp
    """
    if coords is None:
        res = slice(None)
    elif isinstance(coords, Iterable) and len(coords) in (2, 3):
        res = slice(*coords)
    elif isinstance(coords, slice):
        res = coords
    else:
        raise ValueError("Not implemented for coords={0}".format(coords))
        
    return res

def rad2deg(radians):
    """radians to degree conversion"""
    degrees = 180 * radians / math.pi
    # ~ raise ValueError("Deprecated: use math")
    return degrees

def deg2rad(degrees):
    """deg to radians conversion"""
    radians = math.pi * degrees / 180
    # ~ raise ValueError("Deprecated: use math")
    return radians

def rotated_coords(lons, lats, lonpole, latpole, pole="North", direction="rot2reg"):
    """
    >>> from japet_misc import norm_numb
    >>> sp_lon = 18 
    >>> sp_lat = -39.3
    >>> lats = numpy.array([53, 54, 55])
    >>> lons = numpy.array([12]*3)
    >>> rdic= rotated_coords(lons, lats, sp_lon, sp_lat, pole = 'South', direction = "reg2rot")
    >>> lons, lats = [rdic[i] for i in ('lons', 'lats')]
    >>> norm_numb(lons, '.4f')
    [-3.61, -3.5289, -3.4476]
    >>> norm_numb(lats, '.4f')
    [2.4463, 3.443, 4.4397]
    >>> ndic = rotated_coords(lons, lats, sp_lon, sp_lat, pole = 'South', direction = "rot2reg")
    >>> lons, lats = [ndic[i] for i in ('lons', 'lats')]
    >>> norm_numb(lons, '.4f')
    [12.0, 12.0, 12.0]
    >>> norm_numb(lats, '.4f')
    [53.0, 54.0, 55.0]

    >>> lats = numpy.array([-21.34, 5.9400001])
    >>> lons = numpy.array([-23.219999, 19.459999])
    >>> nplon = -162.0
    >>> nplat = 39.25
    
    >>> rdic= rotated_coords(lons, lats, nplon, nplat, pole = 'North', direction = "rot2reg")
    >>> lons, lats = [rdic[i] for i in ('lons', 'lats')]
    >>> norm_numb(lons, '.4f')
    [-6.0368, 50.8483]
    >>> norm_numb(lats, '.4f')
    [25.6345, 52.3457]

    >>> ndic = rotated_coords(lons, lats, nplon, nplat, pole = 'North', direction = "reg2rot")
    >>> lons, lats = [ndic[i] for i in ('lons', 'lats')]
    >>> norm_numb(lons, '.4f')
    [-23.22, 19.46]
    >>> norm_numb(lats, '.4f')
    [-21.34, 5.94]
    """
    #pole infos
    
    if pole == "North":
        sp_lon = lonpole + 180
        sp_lat = - latpole
    elif pole == "South":
        sp_lon = lonpole
        sp_lat = latpole
    else:
        assert 0, "pb input"
    
    theta = 90 + sp_lat     # Rotation around y-axis
    phi = sp_lon          # Rotation around z-axis

    phi = deg2rad(phi)    # Convert degrees to radians
    theta = deg2rad(theta)


    lons = deg2rad(lons)   # Convert degrees to radians
    lats = deg2rad(lats)

    # Convert from spherical to cartesian coordinates
    x = numpy.cos(lons) * numpy.cos(lats) 
    y = numpy.sin(lons) * numpy.cos(lats)
    z = numpy.sin(lats)


    if direction == "reg2rot":      #Regular -> Rotated
        x_new = numpy.cos(theta) *numpy.cos(phi) *x + numpy.cos(theta)*numpy.sin(phi)*y + numpy.sin(theta)*z
        y_new = -numpy.sin(phi)*x + numpy.cos(phi) * y
        z_new = - numpy.sin(theta) * numpy.cos(phi) * x - numpy.sin(theta) * numpy.sin(phi) *y + numpy.cos(theta) *z
        
    elif direction == "rot2reg":    #Rotated -> Regular
        phi = -phi
        theta = -theta

        x_new = numpy.cos(theta) * numpy.cos(phi) * x + numpy.sin(phi)*y + numpy.sin(theta) * numpy.cos(phi)*z
        y_new = - numpy.cos(theta) * numpy.sin(phi) * x + numpy.cos(phi) * y - numpy.sin(theta) * numpy.sin(phi) *z
        z_new = -numpy.sin(theta) * x + numpy.cos(theta) * z        

    else:
        assert 0

    # Convert cartesian back to spherical coordinates

    lon_new = [math.atan2(i_y, i_x) for i_y, i_x in zip(y_new, x_new)]
    lat_new = [math.asin(i_z) for i_z in z_new]

    lon_new = numpy.array(lon_new)
    lat_new = numpy.array(lat_new)

    lon_new = rad2deg(lon_new) # Convert radians back to degrees
    lat_new = rad2deg(lat_new)
    
    res = {"lons" : lon_new, "lats" : lat_new}
    
    return res


##======================================================================================================================##
##                PROJ DIC CLASS                                                                                        ##
##======================================================================================================================##


class Proj_Dic:
    """ Class doc
    >>> dic = {dim : {i:i+1 for i in range(10)} for dim in ("x", "y")}
    >>> pdic = Proj_Dic(dic, regular = True) 
    >>> pdic(1,1)
    (2, 2)
    >>> pdic(1,1.4)
    Traceback (most recent call last):
            ...
    AssertionError: y : 1.4 not available
    >>> pdic(9,8)
    (10, 9)
    >>> pdic(10,10)
    Traceback (most recent call last):
            ...
    AssertionError: x : 10 not available
    """
    
    def __init__(self, dic, regular=True):
        """ Class initialiser 
        See class doctest
        """
        assert isinstance(regular, bool), 'pb input, expected {0} but got {1.__class__} : {1}'.format("bool", regular)
        assert isinstance(dic, dict), 'pb input, expected {0} but got {1.__class__} : {1}'.format("dict", dic)
        self.regular = regular
        self.dic = self._check(dic)
            
    def _check(self, dic):
        """
        See class doctest
        """
        if self.regular:
            for i in ("x", "y"):
                assert i in dic, "pb keys, {0} not in dic".format(i)
                assert all([isinstance(j, (int, float)) for j in dic[i].keys()]), 'pb input, expected numbs'
                assert all([isinstance(j, (int, float)) for j in dic[i].values()]), 'pb input, expected numbs'                        
        else:
            for k, v in dic.items():
                for i in k, v: 
                    assert isinstance(i, tuple), 'pb input, expected {0} but got {1.__class__} : {1}'.format("tuple", i)
                    assert len(i) == 2, 'pb input, expected {0} but got {1.__class__} : {1}'.format("len == 2", i)
                 
        res = dic.copy()
        
        return res
            
    def __call__(self, x, y):
        """
        See class doctest
        """
        assert isinstance(x, (int, float)) and isinstance(y, (int, float)), "x and y must be number"
        
        if self.regular:
            assert x in self.dic["x"], "x : {0} not available".format(x)
            assert y in self.dic["y"], "y : {0} not available".format(y)
            xn = self.dic["x"][x]
            yn = self.dic["y"][y]
                    
            
            res = (xn, yn)
        else:
            xy = (x, y)
            assert xy in self.dic, "{0} not available".format(xy)
            
            res = self.dic[xy]

        return res
        
        
##======================================================================================================================##
##                LONGITUDES CLASS                                                                                      ##
##======================================================================================================================##

class Longitudes(pandas.Series):
    """ Class doc
    >>> float(Longitudes(0,"W").new_orientation("EW")) == 0.
    True
    >>> float(Longitudes(-10,"EW").new_orientation("W")) == 350.
    True
    >>> float(Longitudes(350,"W").new_orientation("EW")) == -10.
    True
    >>> float(Longitudes(181,"W").new_orientation("EW")) == -179.
    True
    >>> float(Longitudes(-179,"EW").new_orientation("W")) == 181.
    True
    >>> float(Longitudes(-1,"EW").new_orientation("W")) == 359.
    True
    >>> float(Longitudes(-0.1,"EW").new_orientation("W")) == 359.9
    True
    >>> float(Longitudes(359,"W").new_orientation("EW")) == -1.
    True
    >>> float(Longitudes(359.5,"W").new_orientation("EW")) == -0.5
    True
    
    >>> lons = Longitudes(numpy.arange(0, 360, 60))
    >>> lons.orientation
    'W'
    >>> all(lons.new_orientation("EW") == numpy.array([   0.,   60.,  120., -180., -120.,  -60.]))
    True

    >>> all(lons.new_orientation("W") == numpy.array([  0.,  60., 120., 180., 240., 300.]))
    True
    
    >>> lons = Longitudes(numpy.arange(-180, 180, 60))
    >>> lons.orientation
    'EW'
    >>> all(lons.new_orientation("EW") == numpy.array([-180., -120.,  -60.,    0.,   60.,  120.]))
    True
    >>> all(lons.new_orientation("W") == numpy.array([180., 240., 300.,   0.,  60., 120.]))
    True
    """
    
    def __init__(self, lons, orientation=None):
        """ Class initialiser """
        pandas.Series.__init__(self, lons, dtype=float)
        
        if orientation is None:
            self.orientation = self.detect_orientation()
        else:
            assert orientation in OKEYS
            self.orientation = orientation
            
        self._check()
            
    def _check(self):
        """
        Check if orientation and value are consistent
        """
        mnlon = self.min()
        mxlon = self.max()
        if self.orientation == WKEY:
            assert mnlon >= 0 and mxlon < 360
        elif self.orientation == EWKEY:
            assert mnlon >= -180 and mxlon < 180
        elif self.orientation == EKEY:
            assert mnlon >= -360 and mxlon < 0
        else:
            raise ValueError("pb code")
        
    def detect_orientation(self):
        """
        Detect the orientation
        """
        mnlon = self.min()
        mxlon = self.max()
        
        if mnlon < -180:
            res1 = [EKEY]
        elif mnlon < 0:
            res1 = [EKEY, EWKEY]
        elif mnlon == 0:
            res1 = [EKEY, EWKEY, WKEY]
        else:
            res1 = [EWKEY, WKEY]
            
        if mxlon > 180:
            res2 = [WKEY]
        elif mxlon > 0:
            res2 = [WKEY, EWKEY]
        elif mxlon == 0:
            res2 = [EKEY, EWKEY, WKEY]
        else:
            res2 = [EKEY]
        
        #~ print(res1, res2)
        
        res = list(set.intersection(set(res1), set(res2)))
        #~ print(res)
        if not res:
            raise ValueError("incoherent longitudes")
        elif len(res) == 1:
            res = res[0]
        else:
            print("Can not determine actual orientation : {0}".format(res))
            res = None
            
        return res
        
    def new_orientation(self, new):
        """change lngitudes to wew orientation, see main doctest"""
        values = self.values
        
        if new == self.orientation:
            res = values
        elif self.orientation == EWKEY and new == WKEY:
            res = values % 360
        elif self.orientation == WKEY and new == EWKEY:
            res = ((values - 180) % 360) - 180
        else:
            raise ValueError("not implemented")
        
        Longitudes(res, orientation=new) #check
        
        res = numpy.array(res, dtype=float)
        
        return res


   
##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':

    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)


    lons1 = numpy.arange(-180, 180)
    lons2 = numpy.arange(0, 360)


    lons1 = Longitudes(lons1)
    lons2 = Longitudes(lons2)
