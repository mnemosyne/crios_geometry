"""
Points 
"""


##======================================================================================================================##
##                PACKAGE IMPORT                                                                                        ##
##======================================================================================================================##

import argparse
from collections.abc import Iterable
from numbers import Number
import math

import numpy
import pandas
import scipy.stats

from matplotlib import pyplot

import shapely
import shapely.geometry


from crios_geometry.cst import XKEY, YKEY, ZKEY
from crios_geometry.utils import XYlims
from crios_geometry.spatial.objects import Point, distance_2pts, Polygon


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

DEFMARKER = {'mec' : 'k', 'mfc' : "w", 'marker' : "o", "mew" : 1, "ls" : "", "ms" : 8}


##======================================================================================================================##
##                EXAMPLE                                                                                               ##
##======================================================================================================================##

def example_points(n_point, random=True, texts=False):
    """
    >>> example_points(10).__class__ == Points
    True
    """
    x = scipy.stats.norm().rvs(n_point)
    y = scipy.stats.norm().rvs(n_point)

    if texts:
        vs = ['er', 'som', 'pla', 'gre']
        z = [vs[i%4] for i in range(n_point)]
    else:
        if random:
            z = scipy.stats.norm().rvs(n_point)
        else:
            z = x + y

    res = Points(x=x, y=y, z=z)

            
    return res


##======================================================================================================================##
##                POINTS CLASS                                                                                          ##
##======================================================================================================================##


class Points(pandas.Series):
    """ Class doc
    >>> from pyproj import Proj
    >>> from japet_misc import *
    >>> import matplotlib
        
    >>> xs = numpy.arange(10)
    >>> ys = xs*2
    >>> zs = xs**2.
    >>> pts = Points(xs, ys, zs)
    >>> pts    
    (0.0, 0.0)      0.0
    (1.0, 2.0)      1.0
    (2.0, 4.0)      4.0
    (3.0, 6.0)      9.0
    (4.0, 8.0)     16.0
    (5.0, 10.0)    25.0
    (6.0, 12.0)    36.0
    (7.0, 14.0)    49.0
    (8.0, 16.0)    64.0
    (9.0, 18.0)    81.0
    dtype: float64

    >>> pts.get_coordinates()
                   x     y
    (0.0, 0.0)   0.0   0.0
    (1.0, 2.0)   1.0   2.0
    (2.0, 4.0)   2.0   4.0
    (3.0, 6.0)   3.0   6.0
    (4.0, 8.0)   4.0   8.0
    (5.0, 10.0)  5.0  10.0
    (6.0, 12.0)  6.0  12.0
    (7.0, 14.0)  7.0  14.0
    (8.0, 16.0)  8.0  16.0
    (9.0, 18.0)  9.0  18.0

    >>> bmap=Proj(llcrnrlon=0, llcrnrlat=0, urcrnrlon=1, urcrnrlat=1, proj='merc', lon_0=0, lat_0=0,ellps = 'WGS84')
    >>> npts = pts.use_projection(bmap)
    >>> npts.lims()
                    x             y
    min  0.000000e+00  0.000000e+00
    max  1.001875e+06  2.024351e+06
    >>> ii = numpy.arange(5)
    >>> pts=Points(ii,ii)
    >>> dists = pts.distances(1,1.5)
    >>> dists
    (0.0, 0.0)    1.802776
    (1.0, 1.0)    0.500000
    (2.0, 2.0)    1.118034
    (3.0, 3.0)    2.500000
    (4.0, 4.0)    3.905125
    dtype: float64

    dtype: float64
    >>> ["{0:.1f}".format(i) for i in dists]
    ['1.8', '0.5', '1.1', '2.5', '3.9']
    >>> pts=Points(ii,ii)
    >>> pt = Point(x=1,y=1.5)
    >>> npt = pts.nearest_point(pt)
    >>> npt.x
    1.0
    >>> npt.y
    1.0
    >>> distance_2pts(npt, pt)
    0.5
    >>> vec = numpy.arange(1,5.)
    >>> pts=Points(x = 2* vec,y = -vec)
    >>> pts.lims()
           x    y
    min  2.0 -4.0
    max  8.0 -1.0
    >>> n = 50
    >>> p_l = Points(x=scipy.stats.norm(loc=0, scale=1).rvs(n), y=scipy.stats.norm(loc=6, scale=1).rvs(n), z=scipy.stats.norm().rvs(n))
    >>> f=pyplot.figure()
    >>> r=p_l.plot()
    >>> r=p_l.plot(marker = "s")
    >>> f.show()
    >>> pyplot.close()
    >>> p_l = Points(x=scipy.stats.norm(loc=0, scale=1).rvs(n), y=scipy.stats.norm(loc=6, scale=1).rvs(n), z=scipy.stats.norm().rvs(n))
    >>> lim = normalize_list(p_l.values, factor = 1)
    >>> vn, vx = lim
    >>> bds = numpy.arange(vn, vx, 0.5)
    
    >>> cmnm = 'viridis'
    >>> fig = pyplot.figure()
    >>> axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))
    >>> ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))

    >>> nbds = len(bds)
    >>> ncol = nbds - 1
    >>> cm = get_colormap(name = cmnm, ncol = ncol, extend = True)
    >>> nrm = matplotlib.colors.BoundaryNorm(bds, ncol)
    >>> pc=p_l.scatter(cmap = cm, norm = nrm)
    >>> cb = pyplot.colorbar(cax = axcb, extend = "both")
    >>> f = pyplot.figure()
    >>> axcb = pyplot.axes((0.75, 0.1, 0.08, 0.8))
    >>> ax = pyplot.axes((0.1, 0.1, 0.6, 0.8))
    >>> cb = plot_colorbar(bds, cax = axcb)
    >>> lc = p_l.scatter(colorbar =cb)
    >>> show_and_close()

    >>> f=pyplot.figure()
    >>> ts= example_points(5, texts=True).annotate_plot()
    >>> f.show()
    >>> pyplot.close()

    >>> limits = (1, 2)
    >>> mkscale = MarkerScale(*limits)
    >>> mkscale = mkscale.add_property(ms = [8,10,12])
    >>> mkscale = mkscale.add_property(marker = ["s","o","^"],mfc = ["r","k","c"])
    >>> mkscale = mkscale.add_property(mew = 2)
    >>> a = mkscale.plot()
    >>> lons = lats = numpy.arange(0,3, 0.5)
    >>> pts = Points(lons, lats, lons * lats)
    >>> f = pyplot.figure()
    >>> lines = pts.marker_plot(mkscale)
    >>> f.show()
    >>> pyplot.close()
    
    >>> dists = pts.inter_distances()
    >>> dists.size == numpy.arange(lons.size).sum()
    True
    >>> format(dists.min(), ".2f")
    '0.71'
    
    >>> (pts.spatial_distinction() == pts).all()
    Spatial distinction stoped at iter 0  
    Number of changing points = 0
    True
    
    >>> pts2 = Points(pandas.concat([pts]*2))
    >>> pts2
    (0.0, 0.0)    0.00
    (0.5, 0.5)    0.25
    (1.0, 1.0)    1.00
    (1.5, 1.5)    2.25
    (2.0, 2.0)    4.00
    (2.5, 2.5)    6.25
    (0.0, 0.0)    0.00
    (0.5, 0.5)    0.25
    (1.0, 1.0)    1.00
    (1.5, 1.5)    2.25
    (2.0, 2.0)    4.00
    (2.5, 2.5)    6.25
    dtype: float64
    >>> pts3 = pts2.spatial_distinction()
    Spatial distinction stoped at iter 6  
    Number of changing points = 12
    >>> pts3
    (0.0001, 0.0001)      0.00
    (0.5001, 0.5001)      0.25
    (1.0001, 1.0001)      1.00
    (1.5001, 1.5001)      2.25
    (2.0001, 2.0001)      4.00
    (2.5001, 2.5001)      6.25
    (-0.0001, -0.0001)    0.00
    (0.4999, 0.4999)      0.25
    (0.9999, 0.9999)      1.00
    (1.4999, 1.4999)      2.25
    (1.9999, 1.9999)      4.00
    (2.4999, 2.4999)      6.25
    dtype: float64
    >>> pts4 = pts3.extraction(xlim=(0.5, 2))
    >>> pts5 = pts4.extraction(ylim=(1.5, 2))
    >>> pts4
    (0.5001, 0.5001)    0.25
    (1.0001, 1.0001)    1.00
    (1.5001, 1.5001)    2.25
    (0.9999, 0.9999)    1.00
    (1.4999, 1.4999)    2.25
    (1.9999, 1.9999)    4.00
    dtype: float64
    >>> pts5
    (1.5001, 1.5001)    2.25
    (1.9999, 1.9999)    4.00
    dtype: float64
    >>> pts3.extraction(xlim=(0.5, 2), ylim=(1.5, 2))
    (1.5001, 1.5001)    2.25
    (1.9999, 1.9999)    4.00
    dtype: float64
    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        #~ print(args, len(args))
        #~ print(kwargs, len(kwargs))
        
        if len(args) == 1 and not kwargs:
            obj = args[0]
            assert isinstance(obj, (pandas.Series, Points))
            pandas.Series.__init__(self, obj)
            #~ assert 0, "to implement"
        else:
            if len(args) == 2:
                x, y = args
                z = [None] * len(x)
            elif len(args) == 3:
                x, y, z = args
            elif XKEY in kwargs and YKEY in kwargs:
                x = kwargs[XKEY]
                y = kwargs[YKEY]
                if ZKEY in kwargs:
                    z = kwargs[ZKEY]
                else:
                    z = [None] * len(x)
            else:
                raise ValueError("Dont know what to do with {0}\n{1}".format(args, kwargs))
            
            if all(isinstance(i, Number) for i in (x, y, z)):
                x = [x]
                y = [y]
                z = [z]
            else:
                assert all(isinstance(i, Iterable) for i in (x, y, z))
            
            x = numpy.array(x, dtype=float)
            y = numpy.array(y, dtype=float)
            
            if all(isinstance(i, Number) for i in z):
                z = numpy.array(z, dtype=float)
            
            
            assert len(x) == len(y) == len(z), "exp same lengths for x, y, z"
            
            pandas.Series.__init__(self, z, index=list(zip(x, y)))
        
        
    def get_coordinates(self):
        """Return a dataframe containing the x,y coordinates"""
        x = [i[0] for i in self.index]
        y = [i[1] for i in self.index]
        
        res = pandas.DataFrame({XKEY : x, YKEY : y}, index=self.index)
        return res

    def use_projection(self, proj_obj):
        """Project the points"""
        df = self.get_coordinates()
        xs = df[XKEY].values
        ys = df[YKEY].values
        
        xyproj = proj_obj(xs, ys)
        xsproj, ysproj = xyproj
        
        res = Points(x=xsproj, y=ysproj, z=self.values)
        
        return res
        

    def distances(self, *args):
        """Compute distance in comparison with poinnt given"""
        if len(args) == 1:
            args = args[0]
            assert isinstance(args, Point), f"Point expected got {args}"
            x = args.x
            y = args.y
        elif len(args) == 2:
            x, y = args
        else:
            assert 0, f"Exp point or x, y values, got {args}"
        
        df = self.get_coordinates()
        xs = df[XKEY]
        ys = df[YKEY]
        
        dx = xs - x
        dy = ys - y
        
        # ~ dxdy = pandas.concat([dx, dy], axis=1)
        # ~ print(dxdy)
        # ~ res = dxdy.apply(lambda row: math.hypot(*row), axis=1)
        res = numpy.hypot(dx, dy)
        # ~ print(res)
        return res
        
    def inter_distances(self):
        """
        Compute all distances
        """
        coords = self.get_coordinates()
        #
        npt = coords.index.size

        ipts1 = [ipt1 for ipt1 in range(npt) for ipt2 in range(ipt1 + 1, npt)]
        ipts2 = [ipt2 for ipt1 in range(npt) for ipt2 in range(ipt1 + 1, npt)]


        idx = [tuple([p1, p2]) for p1, p2 in zip(ipts1, ipts2)]
        pts1 = coords.iloc[ipts1]
        pts2 = coords.iloc[ipts2]

        pts1 = pandas.DataFrame(pts1.values, columns=pts1.columns, index=idx)
        pts2 = pandas.DataFrame(pts2.values, columns=pts2.columns, index=idx)
        dxdy = pts1 - pts2
        res = dxdy.apply(lambda row: math.hypot(*row), axis=1)
        return res
                
    def spatial_distinction(self, min_dist=1e-4):
        """
        Separate artificially points that are located at the same locations
        """
        assert min_dist > 0, "min distance must be higher than 0"
        zs = self.values
        res = Points(self)
        
        add_dist = min_dist # add it into options if necessary
        #~ coords = self.get_coordinates()
        #~ dists = self.inter_distances()
        iiter = 0
        _maxiter = 1000

        for iiter in range(_maxiter):
            dists = res.inter_distances()
            if dists.min() <= min_dist:
                coords = res.get_coordinates()
                xs = numpy.array(coords[XKEY], dtype=float)
                ys = numpy.array(coords[YKEY], dtype=float)
                idmin = dists.idxmin()
                i1, i2 = idmin
                #~ print(i1, i2)    
                assert xs[i1] == xs[i2]
                assert ys[i1] == ys[i2]
                xs[i1] = xs[i1] + add_dist
                ys[i1] = ys[i1] + add_dist
                xs[i2] = xs[i2] - add_dist
                ys[i2] = ys[i2] - add_dist    
                res = Points(x=xs, y=ys, z=zs)
            else:
                print("Spatial distinction stoped at iter", iiter, " \nNumber of changing points =", 2*iiter)
                break
        #~ raise

        coords1 = self.get_coordinates()
        coords2 = res.get_coordinates()
        max_displacement = numpy.abs((coords1.values - coords2.values).flatten()).max()
        assert max_displacement <= 5*add_dist, f"Max displacement = {max_displacement}"
        assert (res.values == self.values).all()
        assert self.index.size - (self.index == res.index).sum() <= 2 * iiter
        assert res.inter_distances().min() >= min_dist, f"optimization failed. iteration = {iiter+1}/{_maxiter}"

        return res
        
        
                
    def nearest_point(self, *args):
        """Return the nearest point"""
        dists = self.distances(*args)
        # ~ print(dists)
        xymin = dists.idxmin()
        # ~ print(xymin)
        x, y = xymin
        res = Point(x=x, y=y)
        return res

    def lims(self, **kwargs):
        """Return the min and max of x and y"""
        df = self.get_coordinates()
        res = XYlims(df=df, **kwargs)
        return res        

    def plot(self, **kwargs):
        """Plot markers"""
        argdic = DEFMARKER.copy()
        argdic.update(kwargs)
        
        df = self.get_coordinates()

        res = pyplot.plot(df[XKEY], df[YKEY], **argdic)[0]
        return res


    def scatter(self, **kwargs):
        """Plot by using colorbar"""
        if "colorbar" in kwargs:
            #~ assert len(kwarg) == 1
            cb = kwargs.pop("colorbar")
            kwargs.update(dict(cmap=cb.cmap, norm=cb.norm))
        
        argdic = {"alpha" : 1, "s" : 100, "lw" : 1}
        argdic.update(kwargs)
        
        df = self.get_coordinates()
        res = pyplot.scatter(x=df[XKEY], y=df[YKEY], c=self.values, **argdic)
        return res


    def annotate_plot(self, fmt="", short=False, **kwargs):
        """Plot texts"""
        df = self.get_coordinates()
        x = df[XKEY]
        y = df[YKEY]
        
        txts = ["{0:{1}}".format(i, fmt) for i in self.values]
            
        if not short:
            pass
        elif short is True:
            txts = [v[:3] for v in txts]
        elif isinstance(short, int):
            txts = [v[:short] for v in txts]
        else:
            raise ValueError(f"Exp True, False or int, got {short}")
            
        if "xoffset" in kwargs:
            dx = kwargs.pop("xoffset")
            x = x + dx
        
        if "yoffset" in kwargs:
            dy = kwargs.pop("yoffset")
            y = y + dy
            
            
        argdic = {'ha' : 'center', 'va' : 'center', 'fontsize' : 12.}
        argdic.update(kwargs)
        
        res = [pyplot.text(ix, iy, iv, **argdic) for ix, iy, iv in zip(x, y, txts)]

        return res
            
            
    def marker_plot(self, marker_scale): # , add_label=True,**kwargs
        """Plot by using mmarker scale"""
        #~ assert isinstance(marker_scale, Marker_Scale), 'exp mk_scale obj but got {0.__class__}:{0}'.format(marker_scale)
        df = self.get_coordinates()
        x = df[XKEY]
        y = df[YKEY]

        res = [pyplot.plot(ix, iy, **marker_scale.return_mk(iv)) for ix, iy, iv in zip(x, y, self.values)]
        
        return res

    def extraction(self, xlim=None, ylim=None, polygon=None):
        """
        Extract points given limits and constraints
        """
        coords = self.get_coordinates()

        if polygon:
            assert isinstance(polygon, (Polygon, shapely.geometry.Polygon))
            polyoks = coords.apply(lambda pt: polygon.contains(Point(x=pt[XKEY], y=pt[YKEY])), axis=1) 
            # ~ polyoks = polyoks[polyoks]
            res = self[polyoks]
        else:
            res = self.copy()

            
            
        if xlim:
            assert isinstance(xlim, Iterable) and len(xlim) == 2
            x1, x2 = xlim
            xoks = coords.apply(lambda pt: pt[XKEY] >= x1 and pt[XKEY] <= x2 , axis=1)
            # ~ xoks = xoks[xoks]
            res = res[xoks]

        if ylim:
            assert isinstance(ylim, Iterable) and len(ylim) == 2
            y1, y2 = ylim
            yoks = coords.apply(lambda pt: pt[YKEY] >= y1 and pt[YKEY] <= y2 , axis=1)
            # ~ yoks = yoks[yoks]
            res = res[yoks]
        
        res = Points(res)
        return res

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    parser.add_argument('--scatter', default=False, action='store_true')
    opts = parser.parse_args()
    
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:

        from japet_misc import close_all, plot_colorbar, MarkerScale, auto_bounds, show_all

        close_all()
                
        xx = numpy.arange(10)
        yy = xx + 1
        zz = yy * xx

        pt = Points(xx, yy, zz)
        pt2 = Points(pt)

        print(pt)
        print(pt2)
        print(pt == pt2)
        print(pt * pt2)

        print(pt.get_coordinates())


        bds = auto_bounds(pt)
        
        limits = (10, 50)
        mkscale = MarkerScale(*limits)
        mkscale = mkscale.add_property(marker=["s", "o", "^"], ms=[8, 10, 12], mfc='w', mew=2, mec=["r", "k", "c"])

        fig = pyplot.figure()
        ax = fig.gca()
        pt.plot()

        fig = pyplot.figure()
        cax = pyplot.axes([0.92, 0.1, 0.05, 0.8])
        ax = pyplot.axes([0.1, 0.1, 0.8, 0.8])
        c_b = plot_colorbar(bds, cmap="viridis", cax=cax)
        pt.scatter(colorbar=c_b)
        pt.annotate_plot(fmt=".1f")
        

        fig = pyplot.figure()
        ax = fig.gca()

        pt.marker_plot(marker_scale=mkscale)

        show_all()
        # ~ show_and_close(1)


        pt1 = Point(0.1, 0.1)
        dists1 = pt.distances(pt1)
        npt1 = pt.nearest_point(pt1)
        print(distance_2pts(pt1, npt1))
