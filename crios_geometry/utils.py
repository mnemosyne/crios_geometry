"""
Geometry utils and relations: 
 - 2d
  - 3d, see if there?
"""

##======================================================================================================================##
##                                IMPORT                                                                                ##
##======================================================================================================================##

# ~ from collections.abc import Iterable
import math
import numpy
import pandas

import shapely.geometry

from crios_geometry.cst import XKEY, YKEY, LEFT, RIGTH, UPPER, LOWER, CENTER, XY_METHODS, _xy_sep, CENTROID, XYMAPPER


##======================================================================================================================##
##                EXTRACT PARTICULAR POINT FROM KEYS                                                                    ##
##======================================================================================================================##

def _compute_particular_point_on_axis(values, method):
    """
    See particular_point doctest
    """
    if method in (LOWER, LEFT):
        fun = numpy.min
    elif method in (UPPER, RIGTH):
        fun = numpy.max
    elif method in (CENTER, CENTROID):
        fun = numpy.median
    else:
        raise ValueError(f"Expected lower or upper got {method}")
        
    res = fun(values)
    return res
    
def particular_point(x, y, method):
    """
    >>> s={m: particular_point(numpy.arange(10), numpy.arange(5), m) for m in XY_METHODS}
    >>> pandas.Series(s)
    center center    POINT (4.5 2)
    center left        POINT (0 2)
    center rigth       POINT (9 2)
    lower center     POINT (4.5 0)
    lower left         POINT (0 0)
    lower rigth        POINT (9 0)
    upper center     POINT (4.5 4)
    upper left         POINT (0 4)
    upper rigth        POINT (9 4)
    dtype: object
    """
    assert method in XY_METHODS, f"got {method}.\nExpect one of\n{XY_METHODS}"
    # ~ assert _xy_sep in method, "expect '{0}' to separe x and y method".format(_xy_sep)
    ymethod, xmethod = method.split(_xy_sep)
    xpt = _compute_particular_point_on_axis(values=x, method=xmethod)
    ypt = _compute_particular_point_on_axis(values=y, method=ymethod)
    res = shapely.geometry.Point(xpt, ypt)
    return res
    
    




##======================================================================================================================##
##                                FUNCTIONS                                                                             ##
##======================================================================================================================##

def distance(dx, dy):
    """
    renvoie le distance euclidienne
    >>> distance(3, 4)
    DEPRECATED use math.hypot
    5.0
    >>> all(distance(numpy.array([3, 6]), numpy.array([4, 8])) == numpy.array([ 5., 10.]))
    DEPRECATED use math.hypot
    True
    """
    print("DEPRECATED use math.hypot")
    res = (dx ** 2 + dy ** 2) ** 0.5
    return res




def circle_area(r):
    """
    >>> rpi = math.pi**0.5
    >>> round(circle_area(1/rpi), 0)
    1.0
    >>> round(circle_area(2/rpi), 0)
    4.0
    """
    res = math.pi * r **2
    return res


def sphere_area(r):
    """
    >>> rpi = math.pi**0.5
    >>> carea = circle_area(2/rpi)
    >>> sarea = sphere_area(2/rpi)
    >>> round(sarea, 0)
    16.0
    >>> round(sarea / carea, 0)
    4.0
    >>> earth_surface = sphere_area(6371)
    >>> print(f"Earth surface ~~ {earth_surface/1e6:.0f} millions km^2")
    Earth surface ~~ 510 millions km^2
    >>> print(f"Continenal Earth surface ~~ {earth_surface*0.3e-6:.0f} millions km^2")
    Continenal Earth surface ~~ 153 millions km^2
    """
    res = 4*math.pi * r **2
    return res


##======================================================================================================================##
##                XY LIMS CLASS                                                                                         ##
##======================================================================================================================##

class XYlims(pandas.DataFrame):
    """ Class doc
    >>> df = pandas.DataFrame({"x" : numpy.arange(10), "y" : numpy.arange(15, 25)})
    >>> XYlims(df)
         x   y
    min  0  15
    max  9  24
    >>> XYlims(df, factor= 10)
          x   y
    min   0  10
    max  10  30
    >>> XYlims(df, factor= 5)
          x   y
    min   0  15
    max  10  25
    >>> xyl = XYlims(df, factor= 100)
    >>> xyl
           x    y
    min    0    0
    max  100  100
    
    >>> pandas.DataFrame(xyl)    
           x    y
    min    0    0
    max  100  100
    >>> df = pandas.DataFrame({"lon" : numpy.arange(10), "lat" : numpy.arange(15, 25)})
    >>> XYlims(df)
         lat  lon
    min   15    0
    max   24    9
 
    >>> XYlims(df, factor= 10)
         lat  lon
    min   10    0
    max   30   10
    >>> XYlims(df, factor= 10).apply_mapper()
          x   y
    min   0  10
    max  10  30

    """
    
    def __init__(self, df, factor=None):
        """ Class initialiser """
        mn = pandas.Series(df.min(), name='min')
        mx = pandas.Series(df.max(), name='max')
        if factor is None:
            pass
        else:
            mn = mn.apply(lambda x: (math.floor(x/factor))*factor)
            mx = mx.apply(lambda x: (math.ceil(x/factor))*factor)
        
        res = pandas.concat([mn, mx], axis=1).transpose()
            
        pandas.DataFrame.__init__(self, res, columns=df.columns.sort_values(), index=('min', 'max'))

    def apply_mapper(self):
        """
        Change col names
        """
        new_cols = [XYMAPPER[i] for i in self.columns]
        new_df = pandas.DataFrame(self.values, index=self.index, columns=new_cols)
        res = pandas.DataFrame(new_df, index=new_df.index, columns=(XKEY, YKEY))
        return res

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)


    
