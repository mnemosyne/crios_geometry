"""
Contstants definitions
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

# ~ import math
##======================================================================================================================##
##                CST                                                                                                   ##
##======================================================================================================================##
_xy_sep = " "

LOWER = "lower"
UPPER = "upper"

LEFT = "left"
RIGTH = "rigth"

CENTER = "center"

XY_METHODS = sorted([_xy_sep.join([i, j]) for i in (LOWER, UPPER, CENTER) for j in (LEFT, RIGTH, CENTER)])

XY_CORNERS = [i for i in XY_METHODS if CENTER not in i]
CENTROID = "centroid"
XY_PARTICULAR_POSITIONS = XY_CORNERS + [CENTROID]

XKEY = "x"
YKEY = "y"
ZKEY = "z"
VKEY = 'value'


##======================================================================================================================##
##                MAPS                                                                                                  ##
##======================================================================================================================##

DEGREES_NAMES = ("degree", "deg", "degrees")




# ~ from japet_misc import todo_msg
# ~ print(todo_msg("See which constants is used by a unique file, and move them in it", __file__))

##======================================================================================================================##
##                XYMAPPER                                                                                              ##
##======================================================================================================================##

_xmappers = (XKEY, "longitudes")
_ymappers = (YKEY, "latitudes")

def expand_mappers(mappers):
    """
    Exapnd the possible mappers, see XYLims doctests
    """
    res = [i.lower() for i in mappers] + [i.title() for i in mappers] + [i.upper() for i in mappers]
    res = res + [i[:3] for i in res] + [i[:-1] for i in res]
    res = [i for i in res if i != ""]
    res = sorted(set(res))
    return res

XYMAPPER = {k : val for val, keys in zip([XKEY, YKEY], [_xmappers, _ymappers]) for k in expand_mappers(keys)}
