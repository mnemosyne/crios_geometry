"""
 Input Output
"""



##======================================================================================================================##
##                                IMPORT                                                                                ##
##======================================================================================================================##

import argparse
import doctest
from numbers import Number
import numpy

import shapefile


from crios_geometry.spatial.objects import Line, Polygon, distance_2pts, Point
from crios_geometry.spatial.mapping import Longitudes

##======================================================================================================================##
##                SHAPE FILE READING FUNCTIONS                                                                          ##
##======================================================================================================================##

def shape_to_pts(shape, change_orientation=False):
    """From shape (read in shapefile) to statial points"""
    if change_orientation:
        res = []
        for x, y in shape.points:            
            #~ print(x, end = "-->")
            x = Longitudes(x, orientation="EW").new_orientation("W")
            x = float(x)
            #~ print(x)
            res.append(Point(x, y))
    else:
        res = [Point(x, y) for x, y in shape.points]
    return res

def shape_to_obj(shape, obj="polygon", change_orientation=False, dist_tolerance=0):
    """From shape (read in shapefile) to statial obj : Line or Polygon"""
    pts = shape_to_pts(shape, change_orientation=change_orientation)

    n_pts = len(pts)
    indexes = list(shape.parts) + [n_pts]
    n_parts = len(shape.parts)

    res = []

    for i in range(n_parts):
        ind1 = indexes[i]
        ind2 = indexes[i+1]

        ipts = pts[ind1:ind2]
        
        
        
        if obj in ("poly", "polygon"):
            pt0 = ipts[0]
            ptend = ipts[-1]
            
            if ptend == pt0:
                pass
            else:
                if dist_tolerance is True:
                    dists = [distance_2pts(i,j) for i,j in zip(pts[:-1], pts[1:])]
                    dist_tolerance = numpy.max(dists)
                if dist_tolerance is False:
                    dist_tolerance = 0
                
                assert isinstance(dist_tolerance, Number)
                
                dist_end = distance_2pts(ptend, pt0)
                assert dist_end <= dist_tolerance, f"distance between first and last points is to hight:\n{dist_end} vs {dist_tolerance}"
            ipts = ipts[0:-1]
            iobj = Polygon(ipts)
        else:
            iobj = Line(ipts)
        
        res.append(iobj)
            
    if len(res) == 1:
        res = res[0]

    return res

def shpfile_to_obj(shpfile, **kwargs):
    """
    Read shapefile --> obj
    """
    shreader = shapefile.Reader(shpfile)
    shapes = shreader.shapes()
    res = [shape_to_obj(shape, **kwargs) for shape in shapes]
    if len(res) == 1:
        res = res[0]
        
    return res





##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--example', default=False, action='store_true')
    opts = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        print("Doctests")
        test_result = doctest.testmod()
        print(test_result)
    
           
    #+++++++++++++++++++++++++++++++#
    #        Example                #
    #+++++++++++++++++++++++++++++++#
    if opts.example:
        print("Example")
